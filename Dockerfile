FROM python:3.9

RUN pip3 install poetry
RUN poetry config virtualenvs.create false
RUN apt-get update -y && apt-get install -y gettext

WORKDIR /app
ENV PYTHONPATH=${PYTHONPATH}:${PWD}

COPY pyproject.toml /app
COPY poetry.lock /app

RUN poetry install

COPY . /app

RUN sed -i 's/ALLOWED_HOSTS = \[\]/ALLOWED_HOSTS = ["*"]/g' smartcities_bigdata/settings.py

RUN django-admin compilemessages -l en
RUN django-admin compilemessages -l es
RUN django-admin compilemessages -l pt

RUN mkdir -p data
VOLUME /app/data
RUN mkdir -p static
VOLUME /app/static

EXPOSE 8000
ENTRYPOINT ["/app/bin/entrypoint"]
