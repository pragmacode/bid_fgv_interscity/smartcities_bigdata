# Revision history for Smartcities BigData

The version numbers below try to follow the conventions at http://semver.org/.

Please add new changes under the **Unreleased** section as list items from the newest on top to the oldest.

## Unreleased

* Create about page
* Update GTFS generator scripts from Shapefiles and KMZ to be generic
* Create script to use data from São Paulo frequencies.txt
* Create script to generate Montevideo GTFS
* Create otp_interface show
* Install ModernBusiness template
* Create otp_interface index
* Install django-bootstrap4
* Create otp_interface app (django module)
* Start Waze data collector with docker-compose
* Start OlhoVivo data collector with docker-compose
* Make possible to build graph when starting using Docker Compose
* Bring up webserver using docker compose
* Create Dockerfile
* Get settings from envvars
* Automate all cities OTP startup
* Create OTP Dockerfile
* Create technical report
* Create script to convert KMZ to GTFS
* Create script to convert OGR GeoJSON to GTFS
* Install geopandas
* Create model to persist LineString coordinates from graph generation
* Create model to persist points distances from graph generation
* Convert GTFS to OTP compatible
* Add OTP
* Persist collected Waze jams
* Create models and associations to persist Waze Jams
* Persist collected Waze irregularities
* Create models and associations to persist Waze Irregularities
* Persist collected waze alerts
* Create model to persist Waze Alerts
* Retrieve Waze data
* Save and load graph lines edge from database
* Create model to persist lines edges from graph generation
* Save Graph's ignored lines list to DB
* Load Graph's ignored lines list from DB
* Create model to persist ignored lines from graph generation
* Save and load graph from database
* Create models to persist generated graph
* Check empty inputs for graph building
* Create data samples directory
* Install sentry_sdk
* Create local_settings
* Save Olho Vivo API processed data to MongoDB
* Create BusGraphPosition model
* Use Olho Vivo API to monitor real time data
* Build graph from GTFS
* Remove duplicate vertices for graph building
* Compute bus lines distances for graph building
* Filter out unwanted trips from SP GTFS
* Check intersecting bus lines for graph generation
* Install shapely
* Fetch SP lines from API
* Install requests
* Read SP static GTFS CSV files
* Install pytest-mock
* Install Pandas
* Create GraphGenerator
* Install Djongo
* Install pytest-django
* Install pylama for code linting
* Create initial disaster recovery plan
* Create maintenance plan
* Create empty Django project
