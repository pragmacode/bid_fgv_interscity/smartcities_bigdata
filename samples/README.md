# Smartcities BigData - Data samples

Description for the sample data contained here.

## GTFS

* sp_20210223
  - São Paulo city GTFS downloaded in 23/02/2021
* xalapa.gtfs.zip
  - Generated GTFS using the script described in HACKING.md
* lima.gtfs.zip
  - Generated GTFS using the script described in HACKING.md
