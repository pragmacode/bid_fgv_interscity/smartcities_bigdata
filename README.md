# Smartcities BigData

## Technical Aspects

* On how to install the application, check the deploy [README](deploy/README.md) file
* On how to get started developing, check the [HACKING](HACKING.md) file
* On what changes are part of each release and their compatibility with previous versions, check the
  [CHANGELOG](CHANGELOG.md) file

## Usage Instructions

* For more specific GTFS generation information, refer to its [README.md](./gtfs_generators/README.md) inside `gtfs_generators` folder.


*General Transit Feed Specification (GTFS)* it is a data specification who allows public transportation agencies to
provide their transit data in a way it allows to be read by a wide array of software. It's composed by two parts: the
static one, which contains data regarind calendar, prices and geographic data; and the real-time one which has arrival
estimatives, vehicle position and service warnings.

In this project, we focused in generating the static part, based ondata provided by each city, regarding their public
transportation system. The final goal is to generate OTP compatible GTFS sets.

**São Paulo** already provides a up-to-date GTFS at SPTRANS developer website, but it uses a file that is yet to be
supported by OTP. We developed a script to gather the info from the unsupported file and make it compatile with OTP, for
that, you need to follow those steps:

* be sure that São Paulo GTFS folder contains a file named `frequencies.txt`
* from project root folder, enter `gtfs_generators` folder
* run the following command: `poetry run python sp_use_frequencies.py`
* once finished, you'll have two new files at GTFS folder: `new_stop_times.txt` and `new_trips.txt`
* those files must be put into São Paulo `.gtfs.zip`, taking the place of the originals `trips.txt` and `stop_times.txt`
  (be sure to remove the `new_` prefix before)
* lastly, remove `frequencies.txt` from the `.gtfs.zip`

**Xalapa** doesn't provide a static GTFS, but bus routes info are available at <https://datos.gob.mx/busca/dataset/rutas-tranporte-publico>.
This data is provided in ESRI Shapefiles and we developed a script to generate GTFS from those. just need to run:

`poetry run python gtfs_generators/esri_shapefile_to_gtfs.py <ROOT DIR>`

Where `<ROOT_DIR>` is the folder containing the Shapefiles.

For **Lima** we received directly from a **Miraflores** representative a KML file which has all bus routes from the city.
For that, we also developed a script to generate its GTFS. for run it, do:

`poetry run python gtfs_generators/kml_to_gtfs.py <ROOT_DIR>`

Where `<ROOT_DIR>` is the folder containing the KML file.

For **Montevideo** the data is hosted at <https://catalogodatos.gub.uy/organization/d9026405-35be-410e-b251-492fba99c57d?groups=transporte>.
Once there, download the following files:

* `v_uptu_lsv_destinos.zip`
* `uptu_variante_no_maximal.zip`
* `vuptu_variantes_circulares.zip`
* `v_uptu_paradas.zip`
* `HORARIOS_OMNIBUS datos.zip`
* `uptu_pasada_variante.zip`
* `uptu_pasada_circular.zip`

After that, run the script:

`poetry run python gtfs_generators/montevideo_data_to_gtfs.py <ROOT_DIR>`

Where `<ROOT\_DIR>` is the folder containing the files above.

* For deploying the application with Docker, go to its [README.md](./deploy/README.md) inside `deploy` folder.

Once the application is set up, go to [http://localhost:8000](http://localhost:8000) to access it.


At top-right corner, there is a menu `Routing` which allows choosing the city for which the routes will be generated.

To calculate a route, click on the map once to set the starting point and a second time to set the destination. OTP will
now show all route options matching the given starting and destination points.

In order to collect Waze data for a city other than the ones already collected in the project, edit [smartcities_bigdata/settings.py](./smartcities_bigdata/settings.py) and add that city information to the `WAZE_POLYGONS` dict.

To connect to MongoDB and have the OTP use the data collected from Waze and OlhoVivo, run `docker-compose run mongo mongo`

This should open the command line prompt from MongoDB, so you can do your query. For info regarding database structure,
refer to project files: `./interscitybus/models.py` and `./waze/models.py`.

## License

[Mozilla Public License v2](COPYING)
