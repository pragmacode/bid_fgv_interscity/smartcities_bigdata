# -*- coding: utf-8 -*-

import os
import sys
import pandas as pd
import datetime as dt


def read_frequencies(path):
    return pd.read_csv(path + '/frequencies.txt')


def calculate_partial_starts(hw):
    start_time = dt.time.fromisoformat(hw['start_time'])
    end_time = dt.time.fromisoformat(hw['end_time'])
    delta = dt.timedelta(seconds=hw['headway_secs'])
    trip_starts = []
    time_control = dt.datetime.combine(dt.date.today(), start_time)
    time_limit = dt.datetime.combine(dt.date.today(), end_time)
    while time_control <= time_limit:
        trip_starts.append(time_control.time())
        time_control += delta
    return trip_starts


def generate_trips(freq_df):
    trips = {}
    for _, hw in freq_df.iterrows():
        trip_id = hw['trip_id']
        trips.setdefault(trip_id, {'starts': [], 'info': None})
        partial_starts = calculate_partial_starts(hw)
        trips[trip_id]['starts'] += partial_starts
    return trips


def read_original_trips_info(path, trips):
    trips_info = pd.read_csv(path + '/trips.txt')
    for trip_id in trips:
        trips[trip_id]['info'] = trips_info.loc[trips_info['trip_id'] == trip_id].iloc[0]


def write_new_gtfs_trips(path, trips):
    print_header_bool = True
    mode_char = 'w'
    for trip_id in trips:
        trips_list = []
        seq_trip_id = 1
        for start_time in trips[trip_id]['starts']:
            info = trips[trip_id]['info']
            trips_list.append({
                'route_id': info['route_id'],
                'service_id': info['service_id'],
                'trip_id': info['trip_id'] + '-T' + str(seq_trip_id).zfill(3),
                'trip_headsign': info['trip_headsign'],
                'direction_id': info['direction_id'],
                'shape_id': info['shape_id'],
            })
            seq_trip_id += 1
        trips_df = pd.DataFrame(trips_list)
        trips_df.to_csv(path + '/new_trips.txt', mode=mode_char, index=False, header=print_header_bool)
        print_header_bool = False
        mode_char = 'a'


def stop_times_deltas(path, trips):
    orig_stop_times = pd.read_csv(path + '/stop_times.txt')
    today = dt.date.today()
    for trip_id in trips:
        stops = orig_stop_times.loc[orig_stop_times['trip_id'] == trip_id]
        stops_id_delta = []
        base_datetime = dt.datetime.combine(today, dt.time.fromisoformat(stops.iloc[0]['arrival_time']))
        for _, stop in stops.iterrows():
            stop_datetime = dt.datetime.combine(today, dt.time.fromisoformat(stop['arrival_time']))
            delta = stop_datetime - base_datetime
            stops_id_delta.append({'stop_id': stop['stop_id'], 'delta': delta})
        trips[trip_id]['stops'] = stops_id_delta


def write_new_gtfs_stop_times(path, trips):
    print_header_bool = True
    mode_char = 'w'
    stop_times_deltas(path, trips)
    today = dt.date.today()
    for trip_id in trips:
        seq_trip_id = 1
        stop_times_list = []
        for trip_start_time in trips[trip_id]['starts']:
            base_datetime = dt.datetime.combine(today, dt.time.fromisoformat(str(trip_start_time)))
            ordinal_stop_id = 1
            for stop in trips[trip_id]['stops']:
                stop_times_list.append({
                    'trip_id': trip_id + '-T' + str(seq_trip_id).zfill(3),
                    'arrival_time': (base_datetime + stop['delta']).time(),
                    'departure_time': (base_datetime + stop['delta']).time(),
                    'stop_id': stop['stop_id'],
                    'stop_sequence': ordinal_stop_id
                })
                ordinal_stop_id += 1
            seq_trip_id += 1
        stop_times_df = pd.DataFrame(stop_times_list)
        stop_times_df.to_csv(path + '/new_stop_times.txt', mode=mode_char, index=False, header=print_header_bool)
        print_header_bool = False
        mode_char = 'a'


def main(args):
    print(args[1])
    path = os.path.abspath(args[1])
    freq_df = read_frequencies(path)
    trips = generate_trips(freq_df)
    read_original_trips_info(path, trips)
    write_new_gtfs_trips(path, trips)
    write_new_gtfs_stop_times(path, trips)
    return


main(sys.argv)
