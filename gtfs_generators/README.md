# GTFS generation

## Generate GTFS from ESRI Shapefiles

There are cities that may have their bus lines shapes in ESRI Shapefile format but not a full GTFS. In order to help
them getting started, based in [Xalapa's data](#more-about-xalapa), we have created a script capable of building a valid
GTFS that runs with OTP from an input ESRI Shapefile.

This script makes various assumptions to fill in GTFS required fields for which shapefiles does not contain information:

* `agency.txt` is generated with fictious information
* `calendar.txt` is generated with fictious information in a way that it won't restrict routes
* trip times assume
  - first trip always departs at 6am
  - last trip departs before 9pm
  - trips depart every 15 minutes
  - the travel time between stops is constant in 2 minutes
* if stops positions are not provided, stops will be created every 500m

Run the conversion script with:

```
poetry run python gtfs_generators/esri_shapefile_to_gtfs.py <ROOT DIR>
```

The `<ROOT DIR>` is the path for the directory containing the line folders, i.e. a folder for each line.

Folder name will be used as Line ID.

Each folder must have a `route.zip` file, containing a route shapefile, represented by a linestring and a optional
`stops.zip`, containing a list of stops shapefile, represented by a list of points.

## Generate GTFS from KMZ

There are cities that may have their bus lines shapes in KMZ format but not a full GTFS. In order to help them getting
started, based in [Lima's data](#more-about-lima), we have created a script capable of building a valid GTFS that runs
with OTP from an input KML file extracted for its KMZ container.

This script makes various assumptions to fill in GTFS required fields for which KMZ does not contain information:

* `agency.txt` is generated with fictious information
* `calendar.txt` is generated with fictious information in a way that it won't restrict routes
* trip times assume
  - first trip always departs at 6am
  - last trip departs before 9pm
  - trips depart every 15 minutes
  - the travel time between stops is constant in 2 minutes
* if stops positions are not provided, stops will be created every 500m

Run the conversion script with:

```
poetry run python gtfs_generators/kml_to_gtfs.py <KML_FILE_PATH>
```

The `<KML_FILE_PATH>` is the path for the KML file extracted from its container KMZ. The script will read the <Point>
tag from the KML file, to generate the line paths. This script don't read stops positions, therefore it always assume
that for every bus line, there is a stop every 500m. For this data to be used, the script would need to be tweaked for
it, depending on how those stops are given and linked with the lines.

## Generate GTFS from Montevideo data

The [publicly available datasets](https://catalogodatos.gub.uy/organization/d9026405-35be-410e-b251-492fba99c57d?groups=transporte)
for Montevideo's public transportation is very complete, but it's also very hard to understand, since there are a lot of
different files and some conflicting data and missing/wrong documentation.

Since they are spread CSV and GeoJSON files, we needed to develop a exclusive script for generating Montevideo's GTFS
from those datasets, after being able to understand and relate them.

The good part, is that we almost didn't had the need to assume any data, apart from the service running info, where the
docs listed 3 kinds of service, being: work days (Mon to Fri), Saturdays and Sundays. But some trips had a 4th kind not
matching either of those former 3, so we assumed to have a new running that is "runs every day of the week".

Apart from that, the dataset has full information on stops, trips, timestamps, line names and so on, so this for sure
generates a very accurate OTP instance.

On the bad side, some of those files have conflicting or missing info, likely a product of lack of standardization among
the developers of those datasets, and maybe some outadatings too.

One example of that is regarding the circular trips, that have missing data and some service running ids that didn't
make any sense, for instance:

Service IDs should be an integer in [1, 2, 3], being work days, Saturdays and Sundays, respectively, but a lot of the
circular trips have 0 as data (assumed as "everyday") but even worse, some of them have completely faulty data like 64,
65, that we had no other choice but to disregard them while we don't have more documentation.

One more suggestion to improve even more the accuracy of Montevideo's data is to have data regarding which lines belong
to each of the 4 companies that run buses, since apparently, this contains a color information and could be useful to
users.

Run the conversion script with:

```
poetry run python gtfs_generators/montevideo_data_to_gtfs.py <MONTEVIDEO_DATA_PATH>
```

The `<MONTEVIDEO_DATA_PATH>` is the path for the folder containing the following files from the public website:
* v_uptu_lsv_destinos.zip
* uptu_variante_no_maximal.zip
* v_uptu_variantes_circulares.zip
* v_uptu_paradas.zip
* HORARIOS_OMNIBUS datos.zip
* uptu_pasada_variante.zip
* uptu_pasada_circular.zip


### More about Xalapa

GeoJSON downloaded from: https://datos.gob.mx/busca/dataset/rutas-tranporte-publico

The OSM data was produced from downloading whole Mexico extract
at: https://download.geofabrik.de/north-america/mexico.html , then extracted Xalapa with
[osmium](https://osmcode.org/osmium-tool/manual.html):

```
osmium extract -b -96.7153297,20.132632,-97.400539,19.143240 mexico-latest.osm.pbf -o xalapa.pbf
```

### More about Lima

As far as we know, the KMZ files provided are not publicly available, they were sent directly to us.

The OSM data for Lima was downloaded from https://www.interline.io/osm/extracts/ .


### More about Montevideo

The publicly available datasets are available from: https://catalogodatos.gub.uy/organization/d9026405-35be-410e-b251-492fba99c57d?groups=transporte

The OSM data used was the extract from the entire Uruguay, since it's small enough to run without issues.

Available at: https://download.geofabrik.de/south-america/uruguay.html
