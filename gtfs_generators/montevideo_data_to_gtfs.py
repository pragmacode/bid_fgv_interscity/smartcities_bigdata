# -*- coding: utf-8 -*-

import sys
import pandas as pd
import geopandas as gp
from geopy.distance import geodesic
import datetime as dt
import os


def write_gtfs_agency():
    agency_data_frame = pd.DataFrame([{
        'agency_id': 1,
        'agency_name': 'Sistema de Transporte Metropolitano (STM)',
        'agency_url': 'https://montevideo.gub.uy/areas-tematicas/sistema-de-transporte-metropolitano',
        'agency_timezone': 'America/Montevideo',
        'agency_lang': 'es-419',
        'agency_email': 'administracion.transporte@imm.gub.uy',
    }])
    agency_data_frame.to_csv('./data/montevideo/gtfs/agency.txt', index=False)


def load_geodataframe_input(filepath):
    return gp.read_file('zip://' + filepath, encoding='utf-8')


def read_bus_lines(path):
    folder = path + '/Lineas-origen_destino/'
    lines_gdf = load_geodataframe_input(folder + 'v_uptu_lsv_destinos.zip').to_crs(crs='WGS84')
    nomax_gdf = load_geodataframe_input(folder + 'uptu_variante_no_maximal.zip').to_crs(crs='WGS84')
    circv_gdf = load_geodataframe_input(folder + 'v_uptu_variantes_circulares.zip').to_crs(crs='WGS84')

    lines_info = {}
    for _, data_row in lines_gdf.iterrows():
        line = lines_info.setdefault(int(data_row['COD_LINEA']), {})
        sub_line = line.setdefault(int(data_row['COD_SUBLIN']), {})
        sub_line.setdefault(int(data_row['COD_VARIAN']), []).append(data_row)

    nomax_info = {}
    for _, data_row in nomax_gdf.iterrows():
        nomax_info.setdefault(int(data_row['COD_VARIAN']), []).append(data_row)

    circv_info = {}
    for _, data_row in circv_gdf.iterrows():
        circv_info['C' + str(int(data_row['COD_CIRCUL']))] = data_row

    for var_code in nomax_info:
        for code in lines_info:
            for sub_code in lines_info[code]:
                for var in lines_info[code][sub_code]:
                    if var_code == var:
                        lines_info[code][sub_code][var].append(nomax_info[var_code][0])

    return lines_info, circv_info


def coordinates_distance(coord_a, coord_b):
    swapped_coord_a = (coord_a[1], coord_a[0])
    swapped_coord_b = (coord_b[1], coord_b[0])
    return geodesic(swapped_coord_a, swapped_coord_b).m


def write_gtfs_shapes(lines_info):
    shapes_list = []
    shapes = {}
    shape_id = 1
    for line in lines_info:
        for sub_line in lines_info[line]:
            for variant in lines_info[line][sub_line]:
                variant_shape = {'id': shape_id, 'points': [], 'distances': []}

                point_list = lines_info[line][sub_line][variant][-1][-1].coords
                distance = {'traveled': 0.0, 'head': point_list[0]}
                for index, point in enumerate(point_list):
                    if index < len(point_list):
                        distance['traveled'] += coordinates_distance(distance['head'], point)
                        distance['head'] = point
                    shapes_list.append({
                        'shape_id': shape_id,
                        'shape_pt_lat': point[1],
                        'shape_pt_lon': point[0],
                        'shape_pt_sequence': index + 1,
                        'shape_dist_traveled': distance['traveled'],
                    })
                    variant_shape['points'].append((point[1], point[0]))
                    variant_shape['distances'].append(distance['traveled'])
                shapes[variant] = variant_shape
                shape_id += 1

    shapes_data_frame = pd.DataFrame(shapes_list)
    shapes_data_frame.to_csv('./data/montevideo/gtfs/shapes.txt', index=False)

    return shapes, shape_id


def write_gtfs_circular_shapes(circular_info, shape_id):
    shapes_list = []
    circular_shapes = {}
    for line in circular_info:
        circular_shape = {'id': shape_id, 'points': [], 'distances': []}

        point_list = circular_info[line]['geometry'].coords
        distance = {'traveled': 0.0, 'head': point_list[0]}
        for index, point in enumerate(point_list):
            if index < len(point_list):
                distance['traveled'] += coordinates_distance(distance['head'], point)
                distance['head'] = point
            shapes_list.append({
                'shape_id': shape_id,
                'shape_pt_lat': point[1],
                'shape_pt_lon': point[0],
                'shape_pt_sequence': index + 1,
                'shape_dist_traveled': distance['traveled'],
            })
            circular_shape['points'].append((point[1], point[0]))
            circular_shape['distances'].append(distance['traveled'])

        circular_shapes[line] = circular_shape
        shape_id += 1

    shapes_data_frame = pd.DataFrame(shapes_list)
    shapes_data_frame.to_csv('./data/montevideo/gtfs/shapes.txt', mode='a', header=None, index=False)

    return circular_shapes


def write_gtfs_stops(path):
    stops_gdf = load_geodataframe_input(path + '/paradas-puntos_de_control/v_uptu_paradas.zip').to_crs(crs='WGS84')

    stops_list = {}
    for _, stop in stops_gdf.iterrows():
        stop_values = stops_list.setdefault(stop['COD_UBIC_P'],
                                            {'variant': [], 'ordinal': [], 'point': stop['geometry'],
                                             'name': str(stop['CALLE']) + ' x ' + str(stop['ESQUINA'])})
        stop_values['variant'].append(stop['COD_VARIAN'])
        stop_values['ordinal'].append(stop['ORDINAL'])

    stop_list = []
    for stop in stops_list:
        stop_list.append({
            'stop_id': int(stop),
            'stop_name': stops_list[stop]['name'],
            'stop_lat': stops_list[stop]['point'].y,
            'stop_lon': stops_list[stop]['point'].x,
        })

    stops_data_frame = pd.DataFrame(stop_list)
    stops_data_frame.to_csv('./data/montevideo/gtfs/stops.txt', index=False)


def write_gtfs_routes(lines_info):
    routes_list = []
    for line in lines_info:
        for sub_line in lines_info[line]:
            for variant in lines_info[line][sub_line]:
                routes_list.append({
                    'route_id': variant,
                    'agency_id': 1,
                    'route_short_name': lines_info[line][sub_line][variant][0][2],
                    'route_long_name': lines_info[line][sub_line][variant][0][5],
                    'route_type': 3,
                })

    routes_data_frame = pd.DataFrame(routes_list)
    routes_data_frame.to_csv('./data/montevideo/gtfs/routes.txt', index=False)


def write_gtfs_circular_routes(circular_info):
    routes_list = []
    for line in circular_info:
        cl = circular_info[line]
        long_name = 'CIRC ' + cl['DESC_ORIGE'] + ' - ' + cl['DESC_INTER'] + ' - ' + cl['DESC_DESTI']
        routes_list.append({
            'route_id': line,
            'agency_id': 1,
            'route_short_name': cl['DESC_LINEA'],
            'route_long_name': long_name,
            'route_type': 3,
        })

    routes_data_frame = pd.DataFrame(routes_list)
    routes_data_frame.to_csv('./data/montevideo/gtfs/routes.txt', mode='a', header=None, index=False)


def write_gtfs_calendar():
    start_date = dt.date.today()
    start_date = start_date.replace(year=(start_date.year - 1))
    end_date = dt.date.today()
    end_date = end_date.replace(year=(end_date.year + 1))

    calendar_data = []
    calendar_data.append({
        'service_id': 0,
        'monday': 1, 'tuesday': 1, 'wednesday': 1, 'thursday': 1, 'friday': 1, 'saturday': 1,  'sunday': 1,
        'start_date': start_date.strftime('%Y%m%d'), 'end_date': end_date.strftime('%Y%m%d'),
    })
    calendar_data.append({
        'service_id': 1,
        'monday': 1, 'tuesday': 1, 'wednesday': 1, 'thursday': 1, 'friday': 1, 'saturday': 0,  'sunday': 0,
        'start_date': start_date.strftime('%Y%m%d'), 'end_date': end_date.strftime('%Y%m%d'),
    })
    calendar_data.append({
        'service_id': 2,
        'monday': 0, 'tuesday': 0, 'wednesday': 0, 'thursday': 0, 'friday': 0, 'saturday': 1,  'sunday': 0,
        'start_date': start_date.strftime('%Y%m%d'), 'end_date': end_date.strftime('%Y%m%d'),
    })
    calendar_data.append({
        'service_id': 3,
        'monday': 0, 'tuesday': 0, 'wednesday': 0, 'thursday': 0, 'friday': 0, 'saturday': 0,  'sunday': 1,
        'start_date': start_date.strftime('%Y%m%d'), 'end_date': end_date.strftime('%Y%m%d'),
    })
    calendar_data_frame = pd.DataFrame(calendar_data)
    calendar_data_frame.to_csv('./data/montevideo/gtfs/calendar.txt', index=False)


def get_service_id(code):
    if code == 1 or code == 2:
        return 1
    elif code == 3 or code == 4:
        return 2
    elif code == 5 or code == 6:
        return 3
    else:
        return 0


def get_direction_id(code):
    if code == 'A':
        return 0
    elif code == 'B':
        return 1
    else:
        return 0


def trip_data(data):
    control_dict = {}
    for _, cp in data.iterrows():
        variant = int(cp['Variante'])
        service_id = get_service_id(int(cp['Codigo_minuta']))
        departure_time = int(cp['Nro_frecuencia'])

        control_tuple = (variant, service_id, departure_time)
        value = control_dict.setdefault(control_tuple, None)
        if value is not None:
            continue
        else:
            control_dict[control_tuple] = cp
    return control_dict


def write_gtfs_trips(lines_info, shapes, path):
    data_gdf = load_geodataframe_input(path + '/Horarios-puntos_de_control/HORARIOS_OMNIBUS datos.zip')
    trip_data_atlas = trip_data(data_gdf)
    stops_gdf = load_geodataframe_input(path + '/Horarios-parada/uptu_pasada_variante.zip')
    stops_gdf.drop_duplicates(subset=['cod_variante', 'tipo_dia', 'frecuencia'], keep='first', inplace=True)

    trips_list = []
    seq_trip_id = 1
    trip_id_atlas = {}
    except_dict = {'data': 0, 'info': 0}
    for _, cp in stops_gdf.iterrows():
        variant = int(cp['cod_variante'])
        service_id = int(cp['tipo_dia'])
        departure_time = int(cp['frecuencia'])
        control_tuple = (variant, service_id, departure_time)

        try:
            data = trip_data_atlas[control_tuple]
        except KeyError:
            data = {'Sublinea': 'MISSING HEADSIGN', 'Linea': 'MISSING SHORTNAME'}
            except_dict['data'] += 1

        try:
            info = lines_info[int(data['Cod_linea'])][int(data['Cod_Sublinea'])][variant]
            elem_d = info[-1].to_dict()
        except KeyError:
            elem_d = {'DESC_VARIA': 'A'}
            except_dict['info'] += 1

        trips_list.append({
            'route_id': variant,
            'service_id': service_id,
            'trip_id': seq_trip_id,
            'trip_headsign': data['Sublinea'],
            'trip_shortname': data['Linea'],
            'direction_id': get_direction_id(elem_d['DESC_VARIA']),
            'shape_id': shapes[variant]['id'],
        })
        trip_id_atlas[(variant, service_id, departure_time)] = seq_trip_id
        seq_trip_id += 1

    trips_data_frame = pd.DataFrame(trips_list)
    trips_data_frame.to_csv('./data/montevideo/gtfs/trips.txt', index=False)
    return trip_id_atlas, seq_trip_id


def write_gtfs_circular_trips(circular_info, shapes, path, seq_trip_id):
    stops_gdf = load_geodataframe_input(path + '/Horarios-parada/uptu_pasada_circular.zip')
    stops_gdf.sort_values(by=['cod_circular', 'tipo_dia', 'frecuencia'], inplace=True)
    stops_gdf.drop_duplicates(subset=['cod_circular', 'frecuencia', 'tipo_dia'], keep='first', inplace=True)
    trips_list = []
    circular_trip_id_atlas = {}
    for _, line in stops_gdf.iterrows():
        code = 'C' + str(int(line['cod_circular']))
        try:
            info = circular_info[code]
        except KeyError:
            continue

        try:
            shape = shapes[code]
        except KeyError:
            continue
        else:
            service_id = int(line['tipo_dia'])
            if service_id not in (0, 1, 2, 3):
                continue
            headsign = 'C ' + str(info['DESC_ORIGE']) + '/' + str(info['DESC_INTER']) + '/' + str(info['DESC_DESTI'])
            circular_trip_id_atlas[(line['cod_circular'], line['tipo_dia'], line['frecuencia'])] = seq_trip_id
            trips_list.append({
                'route_id': code,
                'service_id': service_id,
                'trip_id': seq_trip_id,
                'trip_headsign': headsign,
                'trip_shortname': info['DESC_LINEA'],
                'direction_id': 'C',
                'shape_id': shape['id'],
            })
            seq_trip_id += 1

    trips_data_frame = pd.DataFrame(trips_list)
    trips_data_frame.to_csv('./data/montevideo/gtfs/trips.txt', mode='a', index=False, header=None)
    return circular_trip_id_atlas


def fix_time(time_hmm):
    minute = time_hmm % 100
    hour = time_hmm // 100
    return dt.time(hour=hour, minute=minute)


def write_gtfs_stop_times(trip_id_atlas, path):
    stops_gdf = load_geodataframe_input(path + '/Horarios-parada/uptu_pasada_variante.zip')
    stop_times_list = []
    for _, stop in stops_gdf.iterrows():
        trip_id = trip_id_atlas[(int(stop['cod_variante']), int(stop['tipo_dia']), int(stop['frecuencia']))]
        clock = fix_time(int(stop['hora']))
        stop_times_list.append({
            'trip_id': trip_id,
            'arrival_time': clock,
            'departure_time': clock,
            'stop_id': int(stop['cod_ubic_parada']),
            'stop_sequence': int(stop['ordinal'])
        })

    stop_times_data_frame = pd.DataFrame(stop_times_list)
    stop_times_data_frame.to_csv('./data/montevideo/gtfs/stop_times.txt', index=False)


def write_gtfs_circular_stop_times(trip_id_atlas, path):
    stops_gdf = load_geodataframe_input(path + '/Horarios-parada/uptu_pasada_circular.zip')
    stops_gdf.sort_values(by=['cod_circular', 'tipo_dia', 'frecuencia', 'parte', 'ordinal'], inplace=True)
    stop_times_list = []
    for trip_info in trip_id_atlas:
        trips_df = stops_gdf.loc[(stops_gdf['cod_circular'] == trip_info[0])
                                 & (stops_gdf['tipo_dia'] == trip_info[1])
                                 & (stops_gdf['frecuencia'] == trip_info[2])]
        ordinal_stop_id = 1
        for _, stop in trips_df.iterrows():
            clock = fix_time(int(stop['hora']))
            trip_id = trip_id_atlas[(stop['cod_circular'], stop['tipo_dia'], stop['frecuencia'])]
            stop_times_list.append({
                'trip_id': trip_id,
                'arrival_time': clock,
                'departure_time': clock,
                'stop_id': int(stop['cod_ubic_parada']),
                'stop_sequence': ordinal_stop_id
            })
            ordinal_stop_id += 1

    stop_times_data_frame = pd.DataFrame(stop_times_list)
    stop_times_data_frame.to_csv('./data/montevideo/gtfs/stop_times.txt', mode='a', index=False, header=None)


def main(args):
    path = os.path.abspath(args[1])
    write_gtfs_agency()
    write_gtfs_calendar()
    lines_info, circular_info = read_bus_lines(path)
    write_gtfs_routes(lines_info)
    write_gtfs_circular_routes(circular_info)
    shapes, last_shape_id = write_gtfs_shapes(lines_info)
    circular_shapes = write_gtfs_circular_shapes(circular_info, last_shape_id)
    trip_id_atlas, seq_trip_id = write_gtfs_trips(lines_info, shapes, path)
    circular_trip_id_atlas = write_gtfs_circular_trips(circular_info, circular_shapes, path, seq_trip_id)
    write_gtfs_stop_times(trip_id_atlas, path)
    write_gtfs_circular_stop_times(circular_trip_id_atlas, path)
    write_gtfs_stops(path)
    return


main(sys.argv)
