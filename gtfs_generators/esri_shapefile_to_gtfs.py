import sys
import geopandas as gp
import os
from geopy.distance import geodesic
import pandas as pd
import datetime as dt
from shapely.geometry import Point


# pylama:ignore=C901
def create_missing_stops(lines_info, distances):
    stop_id = 1
    stop_ids = []

    for _, routes_stops in lines_info.items():
        if routes_stops['stops'] is None:
            continue

        for _, stop in routes_stops['stops'].iterrows():
            stop_ids.append(stop['id'])

    for line_id, routes_stops in lines_info.items():
        if routes_stops['stops'] is not None:
            continue

        stops = []
        for _, route in routes_stops['route'].iterrows():
            sequence = 1

            while stop_id in stop_ids:
                stop_id += 1
            stop_ids.append(stop_id)

            stops.append({
                'id': stop_id,
                'geometry': Point(route.geometry.coords[0]),
                'sequence': sequence,
                'routeId': route.id,
            })

            sequence += 1
            distance_traveled = 0
            last_stop_distance = 0

            for point_i, distance in enumerate(distances[line_id][route.id]):
                distance_traveled += distance

                if (distance_traveled - last_stop_distance) < 500:
                    continue

                while stop_id in stop_ids:
                    stop_id += 1
                stop_ids.append(stop_id)

                stops.append({
                    'id': stop_id,
                    'geometry': Point(route.geometry.coords[point_i + 1]),
                    'sequence': sequence,
                    'routeId': route.id,
                })

                sequence += 1
                last_stop_distance = distance_traveled

            if (distance_traveled - last_stop_distance) > 0:
                while stop_id in stop_ids:
                    stop_id += 1
                stop_ids.append(stop_id)

                stops.append({
                    'id': stop_id,
                    'geometry': Point(route.geometry.coords[-1]),
                    'sequence': sequence,
                    'routeId': route.id,
                })

        routes_stops['stops'] = gp.GeoDataFrame(stops)


# This is a file required by GTFS specificantion and we'll reference the service_id in trips.txt
#
# Since OGR GeoJSON does not contain it information, here we generate fake information just to comply with these
# requirements
def write_gtfs_calendar():
    service_id = 1

    start_date = dt.date.today()
    start_date = start_date.replace(year=(start_date.year - 1))
    end_date = dt.date.today()
    end_date = end_date.replace(year=(end_date.year + 1))

    calendar_data_frame = pd.DataFrame([{
        'service_id': service_id,
        'monday': 1,
        'tuesday': 1,
        'wednesday': 1,
        'thursday': 1,
        'friday': 1,
        'saturday': 1,
        'sunday': 1,
        'start_date': start_date.strftime('%Y%m%d'),
        'end_date': end_date.strftime('%Y%m%d'),
    }])
    calendar_data_frame.to_csv('calendar.txt', index=False)

    return service_id


# This is a file required by GTFS specificantion and we'll reference the agency_id in routes.txt
#
# Since OGR GeoJSON does not contain it information, here we generate fake information just to comply with these
# requirements
def write_gtfs_agency():
    agency_id = 1

    agency_data_frame = pd.DataFrame([{
        'agency_id': agency_id,
        'agency_name': 'Generated',
        'agency_url': 'http://gtfs.org/',
        'agency_timezone': 'UTC',
    }])
    agency_data_frame.to_csv('agency.txt', index=False)

    return agency_id


def write_gtfs_stop_times(lines_info, trips):
    stop_time_delta = dt.timedelta(minutes=2)
    mode_char = 'w'
    header_bool = True
    for line_id in lines_info:
        route_id = list(trips[line_id].keys())[0]
        for trip in trips[line_id][route_id]:
            dep_time = trip['departure_time']
            stop_times_list = []
            for _, stop in lines_info[line_id]['stops'].iterrows():
                stop_times_list.append({
                    'trip_id': trip['id'],
                    'arrival_time': dep_time.time(),
                    'departure_time': dep_time.time(),
                    'stop_id': stop['id'],
                    'stop_sequence': stop['sequence']
                })
                dep_time += stop_time_delta

            stop_times_data_frame = pd.DataFrame(stop_times_list)
            stop_times_data_frame.to_csv('stop_times.txt', mode=mode_char, index=False, header=header_bool)
            mode_char = 'a'
            header_bool = False


def write_gtfs_trips(lines_info, service_id, shapes):
    trips = {}
    mode_char = 'w'
    header_bool = True

    for line_id, routes_stops in lines_info.items():

        trips[line_id] = {}
        for _, route_info in routes_stops['route'].iterrows():
            trips[line_id][route_info['id']] = []

            first_departure = dt.datetime.now().replace(hour=6, minute=0, second=0, microsecond=0)
            last_departure = dt.datetime.now().replace(hour=20, minute=59, second=59, microsecond=0)
            time = first_departure

            shape_id = shapes[line_id][route_info['id']]['id']

            trip_counter = 0
            trips_list = []
            while time <= last_departure:
                trip_id = route_info['id'] + '-' + str(trip_counter)

                trips_list.append({
                    'trip_id': trip_id,
                    'route_id': route_info['id'],
                    'service_id': service_id,
                    'trip_headsign': route_info['name'],
                    'direction_id': 0,
                    'shape_id': shape_id,
                })

                trips[line_id][route_info['id']].append({
                    'id': trip_id,
                    'departure_time': time,
                })

                trip_counter += 1
                time += dt.timedelta(minutes=15)

            trips_data_frame = pd.DataFrame(trips_list)
            trips_data_frame.to_csv('trips.txt', mode=mode_char, index=False, header=header_bool)
            mode_char = 'a'
            header_bool = False

    return trips


def write_gtfs_routes(lines_info, agency_id):
    route_ids = []
    route_names = []
    mode_char = 'w'
    header_bool = True

    for _, routes_stops in lines_info.items():
        routes_list = []
        for _, route_info in routes_stops['route'].iterrows():
            if route_info['id'] in route_ids:
                continue

            route_ids.append(route_info['id'])

            route_long_name = route_info['desc']
            if route_long_name:
                route_long_name = route_long_name.replace("\n", ' ')

            route_short_name = route_info['name']
            route_counter = 0
            while route_short_name in route_names:
                route_short_name = route_info['name'] + ' ' + str(route_counter)
                route_counter += 1
            route_names.append(route_short_name)

            routes_list.append({
                'route_id': route_info['id'],
                'agency_id': agency_id,
                'route_short_name': route_short_name,
                'route_long_name': route_long_name,
                'route_type': 3,
            })

        routes_data_frame = pd.DataFrame(routes_list)
        routes_data_frame.to_csv('routes.txt', mode=mode_char, index=False, header=header_bool)
        mode_char = 'a'
        header_bool = False


def write_gtfs_stops(lines_info):
    stop_ids = []
    mode_char = 'w'
    header_bool = True

    for _, routes_stops in lines_info.items():
        stops_list = []
        for _, stop_info in routes_stops['stops'].iterrows():
            if stop_info['id'] in stop_ids:
                continue

            stop_ids.append(stop_info['id'])

            stops_list.append({
                'stop_id': stop_info['id'],
                'stop_name': stop_info['id'],
                'stop_desc': stop_info['id'],
                'stop_lat': stop_info['geometry'].y,
                'stop_lon': stop_info['geometry'].x,
            })

        stops_data_frame = pd.DataFrame(stops_list)
        stops_data_frame.to_csv('stops.txt', mode=mode_char, index=False, header=header_bool)
        mode_char = 'a'
        header_bool = False


def write_gtfs_shapes(lines_info, distances):
    shapes = {}
    shapes_list = []
    mode_char = 'w'
    header_bool = True

    shape_id = 1
    for line_id, routes_stops in lines_info.items():
        shapes[line_id] = {}

        for _, route in routes_stops['route'].iterrows():
            route_shape = {'id': int(shape_id), 'points': [], 'distances': []}

            distance_traveled = 0
            shapes_list = []
            for point_i in range(len(route.geometry.coords)):
                point = route.geometry.coords[point_i]

                shapes_list.append({
                    'shape_id': int(shape_id),
                    'shape_pt_lat': float(point[1]),
                    'shape_pt_lon': float(point[0]),
                    'shape_pt_sequence': int(point_i + 1),
                    'shape_dist_traveled': float(distance_traveled),
                })

                route_shape['points'].append((float(point[1]), float(point[0])))
                route_shape['distances'].append(float(distance_traveled))

                if point_i < len(distances[line_id][route.id]):
                    distance_traveled += distances[line_id][route.id][point_i]

            shapes[line_id][route.id] = route_shape

            shape_id += 1

            shapes_data_frame = pd.DataFrame(shapes_list)
            shapes_data_frame.to_csv('shapes.txt', mode=mode_char, index=False, header=header_bool)
            mode_char = 'a'
            header_bool = False

    return shapes


def routes_distances(lines_info):
    distances = {}

    for line_id, routes_stops in lines_info.items():
        distances[line_id] = {}

        for _, route in routes_stops['route'].iterrows():
            route_distances = []

            for point_i in range(len(route.geometry.coords) - 1):
                point_1 = route.geometry.coords[point_i]
                point_2 = route.geometry.coords[point_i + 1]

                point_1_latlon = (point_1[1], point_1[0])
                point_2_latlon = (point_2[1], point_2[0])

                meters_distance = geodesic(point_1_latlon, point_2_latlon).m
                route_distances.append(meters_distance)

            distances[line_id][route.id] = route_distances

    return distances


def read_zip(filepath):
    return gp.read_file('zip://' + filepath)


def load_input(root_dir):
    lines_info = {}
    for folder in os.listdir(os.path.abspath(root_dir)):
        line_path = os.path.abspath(root_dir + '/' + folder)
        if os.path.isfile(line_path + '/route.zip'):
            lines_info[folder] = {'route': read_zip(line_path + '/route.zip')}

            if os.path.isfile(line_path + '/stops.zip'):
                lines_info[folder]['stops'] = read_zip(line_path + '/stops.zip')
            else:
                lines_info[folder]['stops'] = None

    return lines_info


def main(args):
    lines_info = load_input(args[1])

    distances = routes_distances(lines_info)
    create_missing_stops(lines_info, distances)
    shapes = write_gtfs_shapes(lines_info, distances)
    write_gtfs_stops(lines_info)
    agency_id = write_gtfs_agency()
    write_gtfs_routes(lines_info, agency_id)
    service_id = write_gtfs_calendar()
    trips = write_gtfs_trips(lines_info, service_id, shapes)
    write_gtfs_stop_times(lines_info, trips)


main(sys.argv)
