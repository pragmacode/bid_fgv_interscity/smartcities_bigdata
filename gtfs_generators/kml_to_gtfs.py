import sys
from pykml import parser
from shapely.geometry import LineString, Point
import pandas as pd
import geopandas as gp
from geopy.distance import geodesic
import datetime as dt


# pylama:ignore=C901
def create_missing_stops(routes, distances):
    stop_id = 1
    stop_ids = []

    routes_stops = {}
    for route_id in routes:
        stops = []
        sequence = 1

        while stop_id in stop_ids:
            stop_id += 1
        stop_ids.append(stop_id)

        stops.append({
            'id': stop_id,
            'geometry': Point(routes[route_id].coords[0]),
            'sequence': sequence,
            'routeId': route_id,
        })

        sequence += 1
        distance_traveled = 0
        last_stop_distance = 0

        for point_i, distance in enumerate(distances[route_id]):
            distance_traveled += distance

            if (distance_traveled - last_stop_distance) < 500:
                continue

            while stop_id in stop_ids:
                stop_id += 1
            stop_ids.append(stop_id)

            stops.append({
                'id': stop_id,
                'geometry': Point(routes[route_id].coords[point_i + 1]),
                'sequence': sequence,
                'routeId': route_id,
            })

            sequence += 1
            last_stop_distance = distance_traveled

        if (distance_traveled - last_stop_distance) > 0:
            while stop_id in stop_ids:
                stop_id += 1
            stop_ids.append(stop_id)

            stops.append({
                'id': stop_id,
                'geometry': Point(routes[route_id].coords[-1]),
                'sequence': sequence,
                'routeId': route_id,
            })

        routes_stops[route_id] = gp.GeoDataFrame(stops)
    return routes_stops


def write_gtfs_calendar():
    service_id = 1

    start_date = dt.date.today()
    start_date = start_date.replace(year=(start_date.year - 1))
    end_date = dt.date.today()
    end_date = end_date.replace(year=(end_date.year + 1))

    calendar_data_frame = pd.DataFrame([{
        'service_id': service_id,
        'monday': 1,
        'tuesday': 1,
        'wednesday': 1,
        'thursday': 1,
        'friday': 1,
        'saturday': 1,
        'sunday': 1,
        'start_date': start_date.strftime('%Y%m%d'),
        'end_date': end_date.strftime('%Y%m%d'),
    }])
    calendar_data_frame.to_csv('calendar.txt', index=False)

    return service_id


def write_gtfs_agency():
    agency_id = 1

    agency_data_frame = pd.DataFrame([{
        'agency_id': agency_id,
        'agency_name': 'Generated',
        'agency_url': 'http://gtfs.org/',
        'agency_timezone': 'UTC',
    }])
    agency_data_frame.to_csv('agency.txt', index=False)

    return agency_id


def write_gtfs_stop_times(routes, routes_stops, trips):
    stop_time_delta = dt.timedelta(minutes=2)
    mode_char = 'w'
    header_boolean = True
    for route_id in routes:
        for trip in trips[route_id]:
            dep_time = trip['departure_time']
            stop_times_list = []
            for _, stop in routes_stops[route_id].iterrows():
                stop_times_list.append({
                    'trip_id': trip['id'],
                    'arrival_time': dep_time.time(),
                    'departure_time': dep_time.time(),
                    'stop_id': stop['id'],
                    'stop_sequence': stop['sequence']
                })
                dep_time += stop_time_delta

            stop_times_data_frame = pd.DataFrame(stop_times_list)
            stop_times_data_frame.to_csv('stop_times.txt', mode=mode_char, index=False, header=header_boolean)
            mode_char = 'a'
            header_boolean = False


def routes_distances(routes):
    distances = {}

    for route_id in routes:
        route_distances = []
        coord_list = list(routes[route_id].coords)
        for point_i, coord in enumerate(coord_list[:-1]):
            point_1 = coord
            point_2 = coord_list[point_i + 1]

            # coords are in lonlat format
            point_1_latlon = (point_1[1], point_1[0])
            point_2_latlon = (point_2[1], point_2[0])

            meters_distance = geodesic(point_1_latlon, point_2_latlon).m
            route_distances.append(meters_distance)

        distances[route_id] = route_distances

    return distances


def write_gtfs_trips(routes, service_id, shapes):
    trips = {}
    mode_char = 'w'
    header_boolean = True

    for route_id in routes:
        trips[route_id] = []

        first_departure = dt.datetime.now().replace(hour=6, minute=0, second=0, microsecond=0)
        last_departure = dt.datetime.now().replace(hour=20, minute=59, second=59, microsecond=0)
        time = first_departure

        shape_id = shapes[route_id]['id']

        trip_counter = 0
        trips_list = []
        while time <= last_departure:
            trip_id = route_id + '-' + str(trip_counter)
            trips_list.append({
                'trip_id': trip_id,
                'route_id': route_id,
                'service_id': service_id,
                'trip_headsign': route_id + '_trip-headsign',
                'direction_id': 0,
                'shape_id': shape_id,
            })

            trips[route_id].append({
                'id': trip_id,
                'departure_time': time,
            })

            trip_counter += 1
            time += dt.timedelta(minutes=15)

        trips_data_frame = pd.DataFrame(trips_list)
        trips_data_frame.to_csv('trips.txt', mode=mode_char, index=False, header=header_boolean)
        mode_char = 'a'
        header_boolean = False

    return trips


def write_gtfs_routes(routes, agency_id):
    routes_list = []
    for route_id in routes:
        routes_list.append({
            'route_id': route_id,
            'agency_id': agency_id,
            'route_short_name': route_id + ' short_name',
            'route_long_name': route_id + ' long_name',
            'route_type': 3,
        })

    routes_data_frame = pd.DataFrame(routes_list)
    routes_data_frame.to_csv('routes.txt', index=False)


def write_gtfs_stops(routes_stops):
    mode_char = 'w'
    header_boolean = True
    stops_seen = []
    for route_id in routes_stops:
        stops_list = []
        for _, stop in routes_stops[route_id].iterrows():
            if stop['id'] in stops_seen:
                continue
            stops_list.append({
                'stop_id': stop['id'],
                'stop_name': stop['id'],
                'stop_desc': stop['id'],
                'stop_lat': stop['geometry'].y,  # using (y,x) because the data comes in lon-lat
                'stop_lon': stop['geometry'].x,
            })
            stops_seen.append(stop['id'])

        stops_data_frame = pd.DataFrame(stops_list)
        stops_data_frame.to_csv('stops.txt', mode=mode_char, index=False, header=header_boolean)
        mode_char = 'a'
        header_boolean = False


def write_gtfs_shapes(routes):
    shapes = {}
    mode_char = 'w'
    header_boolean = True

    shape_id = 1
    for route_id in routes:
        shapes[route_id] = {}
        points_list = list(routes[route_id].coords)
        route_shape = {'id': shape_id, 'points': [], 'distances': []}

        distance = {'traveled': 0.0, 'head': points_list[0]}
        shapes_list = []
        for index, p in enumerate(points_list):
            if index < len(points_list):
                distance['traveled'] += coordinates_distance(distance['head'], p)
                distance['head'] = p

            shapes_list.append({
                'shape_id': shape_id,
                'shape_pt_lat': p[1],
                'shape_pt_lon': p[0],
                'shape_pt_sequence': index + 1,
                'shape_dist_traveled': distance['traveled'],
            })
            route_shape['points'].append((p[1], p[0]))
            route_shape['distances'].append(distance['traveled'])

        shapes[route_id] = route_shape
        shape_id += 1

        shapes_data_frame = pd.DataFrame(shapes_list)
        shapes_data_frame.to_csv('shapes.txt', mode=mode_char, index=False, header=header_boolean)
        mode_char = 'a'
        header_boolean = False

    return shapes


def coordinates_distance(coord_a, coord_b):
    swapped_coord_a = (coord_a[1], coord_a[0])
    swapped_coord_b = (coord_b[1], coord_b[0])
    return geodesic(swapped_coord_a, swapped_coord_b).m


def load_stops(filename):
    stops_kml = open(filename)
    parsed_stops_kml = parser.parse(stops_kml)
    kml_root = parsed_stops_kml.getroot()
    stops_coordinates = {}
    for child in kml_root.Document.Folder.Placemark:
        stop_id = int(str(child.attrib['id']).split('_')[-1])
        coords = tuple(map(float, str(child.Point.coordinates).split(',')))[:-1]
        stops_coordinates[stop_id] = coords
        # output = open('./data/lima/html/paraderos/' + str(child.attrib['id']) + '.html', 'w')
        # output.write(str(child.description))
    return stops_coordinates


def load_routes(filename):
    routes_kml = open(filename)
    parsed_routes_kml = parser.parse(routes_kml)
    kml_root = parsed_routes_kml.getroot()
    routes = {}
    for child in kml_root.Document.Folder.Placemark:
        coord_list = str(child.MultiGeometry.LineString.coordinates).strip('\n\t ').split(' ')
        linestring_generator = []
        for coord in coord_list:
            coord_tuple = tuple(map(float, coord.split(',')))[:-1]
            linestring_generator.append(coord_tuple)
        routes[str(child.name)] = LineString(linestring_generator)
        # output = open('./data/lima/html/rutas/' + str(child.name) + '.html', 'w')
        # output.write(str(child.description))
    return routes


def main(args):
    routes = load_routes(args[1])
    route_distances = routes_distances(routes)
    routes_stops = create_missing_stops(routes, route_distances)
    write_gtfs_stops(routes_stops)
    shapes = write_gtfs_shapes(routes)
    agency_id = write_gtfs_agency()
    write_gtfs_routes(routes, agency_id)
    service_id = write_gtfs_calendar()
    trips = write_gtfs_trips(routes, service_id, shapes)
    write_gtfs_stop_times(routes, routes_stops, trips)


main(sys.argv)
