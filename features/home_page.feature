Feature: Homepage loading
    In order to ensure that the Homepage loads
    As the Maintainer
    I want to test if the client can access it

    Scenario: Login page loading
        When I visit "/"
        Then it should return a successful response

