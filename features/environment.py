from __future__ import unicode_literals
from selenium import webdriver
from django.conf import settings


def before_all(context):
    settings.DEBUG = True
    context.browser = webdriver.Chrome()
    context.browser.maximize_window()


def after_all(context):
    context.browser.quit()
