from behave import when, then


@when(u'I visit "{path}"')
def visit(context, path):
    context.browser.get(context.base_url + path)


@then(u'it should return a successful response')
def check_not_failed(context):
    assert context.failed is False
