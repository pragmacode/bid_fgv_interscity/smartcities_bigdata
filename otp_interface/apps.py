from django.apps import AppConfig


class OtpInterfaceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'otp_interface'
