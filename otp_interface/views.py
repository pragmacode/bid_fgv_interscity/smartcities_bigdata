from django.shortcuts import render
from django.conf import settings


def index(request):
    return render(request, 'index.html', {})


def show(request, city):
    context = settings.OTP[city]

    return render(request, 'show.html', context)
