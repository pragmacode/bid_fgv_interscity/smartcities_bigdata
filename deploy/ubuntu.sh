#!/bin/bash

set -e
set -v # print commands


# Warning
set +v
printf "**** ATTENTION ****\n\n\tPlease read the instructions in deploy/README.md and the contents of this script before continuing.\n\n\tOnly continue if you are sure that this is the installation procedure you wish to follow.\n\nPress any key to continue or Ctrl+C to exit.\n\n"
read -n 1 k <&1
printf "Continuing...\n\n"
set -v

export DEBIAN_FRONTEND=noninteractive

sudo apt update
sudo apt upgrade -yq


# Python 3.9
sudo apt install -yq wget software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt update
sudo apt install -yq python3.9 python3-pip


# Docker
sudo apt install -yq docker.io
sudo pip3 install docker==5.0.0 docker-compose==1.29.2 websocket-client==0.59.0


# Setup app
sudo apt install -yq osmium-tool zip git build-essential

git clone https://gitlab.com/pragmacode/bid_fgv_interscity/smartcities_bigdata.git
cd smartcities_bigdata

curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3.9 -
source $HOME/.poetry/env
poetry install

mkdir data
mkdir data/mongodb
mkdir data/output


# Setup SP data
mkdir data/sao_paulo

mkdir data/sao_paulo/gtfs

set +v
printf "Download Sao Paulo GTFS available https://sptrans.com.br/desenvolvedores/\n\n\tUnzip its contents to data/sao_paulo/gtfs.\n\nPress any key to continue or Ctrl+C to exit.\n\n"
# cp /vagrant/sao_paulo/gtfs/* data/sao_paulo/gtfs/
# sleep 15
read -n 1 k <&1
printf "Continuing...\n\n"
set -v

ls -l data/sao_paulo/gtfs

mkdir data/sao_paulo/otp
poetry run python3.9 gtfs_generators/sp_use_frequencies.py data/sao_paulo/gtfs/
cp -R data/sao_paulo/gtfs data/sao_paulo/otp/
pushd data/sao_paulo/otp
rm gtfs/frequencies.txt gtfs/stop_times.txt gtfs/trips.txt
mv gtfs/new_stop_times.txt gtfs/stop_times.txt
mv gtfs/new_trips.txt gtfs/trips.txt
zip -rm gtfs.zip gtfs/
popd
rm data/sao_paulo/gtfs/new_trips.txt
rm data/sao_paulo/gtfs/new_stop_times.txt

set +v
printf "Download Sao Paulo OSM extract available at https://www.interline.io/osm/extracts/\n\n\tPlace it at data/sao_paulo/otp.\n\nPress any key to continue or Ctrl+C to exit.\n\n"
# cp /vagrant/sao_paulo/otp/sao-paulo_brazil.osm.pbf data/sao_paulo/otp/
# sleep 15
read -n 1 k <&1
printf "Continuing...\n\n"
set -v

ls -l data/sao_paulo/otp


# Setup Montevideo data
mkdir data/montevideo

mkdir data/montevideo/gtfs

set +v
printf "Download Montevideo transport files available at https://catalogodatos.gub.uy/organization/d9026405-35be-410e-b251-492fba99c57d?groups=transporte\n\n\tUnzip its contents to data/montevideo/gtfs.\n\nPress any key to continue or Ctrl+C to exit.\n\n"
# cp -R /vagrant/montevideo/Horarios-puntos_de_control/ data/montevideo/gtfs/
# cp -R /vagrant/montevideo/Lineas-origen_destino/ data/montevideo/gtfs/
# cp -R /vagrant/montevideo/paradas-puntos_de_control/ data/montevideo/gtfs/
# cp -R /vagrant/montevideo/Horarios-parada/ data/montevideo/gtfs/
# sleep 15
read -n 1 k <&1
printf "Continuing...Hang on this next step may take a few hours\n\n"
set -v

ls -l data/montevideo/gtfs

poetry run python3.9 gtfs_generators/montevideo_data_to_gtfs.py data/montevideo/gtfs

mkdir data/montevideo/otp
cp -R data/montevideo/gtfs/*.txt data/montevideo/otp/
pushd data/montevideo/otp
zip -rm gtfs.zip *.txt
popd

set +v
printf "Download Uruguay OSM extract available at https://download.geofabrik.de/south-america/uruguay.html\n\n\tPlace it at data/montevideo/otp.\n\nPress any key to continue or Ctrl+C to exit.\n\n"
read -n 1 k <&1
# cp /vagrant/montevideo/otp/uruguay-latest.osm.pbf data/montevideo/otp
# sleep 15
printf "Continuing...\n\n"
set -v

ls -l data/montevideo/otp


# Setup Xalapa data
mkdir data/xalapa

mkdir data/xalapa/gtfs

set +v
printf "Download Xalapa transport files available at  https://datos.gob.mx/busca/dataset/rutas-tranporte-publico\n\n\tUnzip its contents to data/xalapa/gtfs.\n\nPress any key to continue or Ctrl+C to exit.\n\n"
# unzip /vagrant/xalapa/shapefiles-mapaton-ciudadano.zip -d data/xalapa/gtfs
# sleep 15
read -n 1 k <&1
printf "Continuing...Hang on this next step may take a few hours\n\n"
set -v

ls -l data/xalapa/gtfs

pushd data/xalapa/gtfs
poetry run python3.9 ../../../gtfs_generators/esri_shapefile_to_gtfs.py shapefiles-mapton-ciudadano/
popd

mkdir data/xalapa/otp
cp -R data/xalapa/gtfs/*.txt data/xalapa/otp/
pushd data/xalapa/otp
zip -rm gtfs.zip *.txt
popd

set +v
printf "Download Mexico OSM extract available at https://download.geofabrik.de/north-america/mexico.html\n\n\tPlace it at data/xalapa/otp.\n\nPress any key to continue or Ctrl+C to exit.\n\n"
read -n 1 k <&1
# cp /vagrant/xalapa/mexico-latest.osm.pbf data/xalapa/otp
# sleep 15
printf "Continuing...\n\n"
set -v

osmium extract -b -96.7153297,20.132632,-97.400539,19.143240 data/xalapa/otp/mexico-latest.osm.pbf -o data/xalapa/otp/xalapa.pbf
rm data/xalapa/otp/mexico-latest.osm.pbf

ls -l data/xalapa/otp


# Setup Lima data
mkdir data/lima

mkdir data/lima/gtfs

set +v
printf "Acquire Lima KMZ transport file.\n\n\tUnzip its contents to data/lima/gtfs/doc.kml.\n\nPress any key to continue or Ctrl+C to exit.\n\n"
# cp /vagrant/lima/doc.kml data/lima/gtfs
# sleep 15
read -n 1 k <&1
printf "Continuing...Hang on this next step may take a few hours\n\n"
set -v

ls -l data/lima/gtfs

pushd data/lima/gtfs
poetry run python3.9 ../../../gtfs_generators/kml_to_gtfs.py doc.kml
popd

mkdir data/lima/otp
cp -R data/lima/gtfs/*.txt data/lima/otp/
pushd data/lima/otp
zip -rm gtfs.zip *.txt
popd

set +v
printf "Download Lima OSM extract available at https://www.interline.io/osm/extracts/\n\n\tPlace it at data/lima/otp.\n\nPress any key to continue or Ctrl+C to exit.\n\n"
read -n 1 k <&1
# cp /vagrant/lima/otp/lima_peru.osm.pbf data/lima/otp
# sleep 15
printf "Continuing...\n\n"
set -v

ls -l data/lima/otp


# Start app
set +v
printf "Edit sudo docker-compose.yml properly setting the variables in the x-django-settings section\n\nThen start the application running:\n\n\tsudo docker-compose up\n\tsudo docker-compose run webserver python manage.py migrate # only for the first time\n\tsudo docker-compose run -v $(pwd)/data:/app/data webserver python manage.py build_graph # only for the first time\n\n\n\nPress any key to continue or Ctrl+C to exit.\n\n"
# cp /vagrant/docker-compose.yml .
# sleep 15
read -n 1 k <&1
printf "Continuing...\n\n"
set -v

# set +v
# sudo docker-compose up -d
# sleep 300
# sudo docker-compose run webserver python manage.py migrate
# sudo docker-compose run webserver python manage.py collectstatic --no-input --clear
# sudo docker-compose run -v $(pwd)/data:/app/data webserver python manage.py build_graph
# set -v
