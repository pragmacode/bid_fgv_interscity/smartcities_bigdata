# Deploying with Docker Compose

As an alternative to the manual setup and configuration steps described at root [HACKING](../HACKING.md#development-environment-configuration),
you can also use Docker Compose to bring up all services in fewer steps.

We recommend performing the setup to be performed manually by an IT professional. In case this is not possible, there is as script for Ubuntu 20.04 automating almost all tasks: [ubuntu.sh](deploy/ubuntu.sh).

You can watch a recording of the installation using this script [here](deploy/demo_ubuntu.mp4). This is a long video, so below we highlight key instants:

* 00:03:10 - dependencies installation
* 00:06:02 - app download and Python dependencies installation
* 00:13:28 - run script to adjust Sao Paulo GTFS to work with OTP
* 00:16:21 - run script to build Montevideo GTFS
* 01:09:56 - run script to build Xalapa GTFS
* 01:11:42 - run script to build Lima GTFS
* 01:27:56 - start application using docker-compose

## Requirements

[Docker Compose](https://docs.docker.com/compose/install/)

## Configuration

Within the repository root, you'll need to have subdiretories structures as follows:

* `data`
  - `sao_paulo`
    * `otp`
      - `*.gtfs.zip` (zipped GTFS files, [warning](../HACKING.md#otp-finds-no-routes))
      - `*.pbf` (OSM extract for the city)
    * `gtfs` (directory containing unzipped GTFS files)
  - `xalapa`
    * `otp`
      - `*.gtfs.zip` (zipped GTFS files)
      - `*.pbf` (OSM extract for the city)
  - `lima`
    * `otp`
      - `*.gtfs.zip` (zipped GTFS files)
      - `*.pbf` (OSM extract for the city)
  - `montevideo`
    * `otp`
      - `*.gtfs.zip` (zipped GTFS files)
      - `*.pbf` (OSM extract for the city)
  - `mongodb` (database will store its files here)

You can find the above mentioned `*.gtfs.zip` already pre-made at the `samples` folder.

Regarding the city extracts, OTP uses OpenStreetMap extracts data provided as .pbf files, we obtained the ones we use at:

* For **São Paulo** and **Lima**, we use the city extracts available at [Interline](https://www.interline.io/osm/extracts/).
* For **Montevideo**, we use the entire Uruguay extract available at [Geofabrik](https://download.geofabrik.de/south-america/uruguay.html),
  since it's small enough to run without issues.
* For **Xalapa** we use an edited version of the Mexico extract available at [Geofabrik](https://download.geofabrik.de/north-america/mexico.html),
  since it's too large to use in its enitrety. To edit it, it's needed to use `osmium` and to run:

  `osmium extract -b -96.7153297,20.132632,-97.400539,19.143240 mexico-latest.osm.pbf -o xalapa.pbf`

Then, open `docker-compose.yml` and review the settings set under the `environment` key (marked with a comment
`CHANGE THIS`).

* `SECRET_KEY`: random secret key which has the function of query validation. Generate it the way you prefer
* `OLHO_VIVO_TOKEN`: obtained from SPTRANS' developer [web-portal](https://sptrans.com.br/desenvolvedores/)
* `SENTRY_DSN`: service-key from the error notification system. Must be obtained by creating an account at [Sentry](https://sentry.io) service
* `WAZE_BASE_URL`: obtained by singning-up on [Waze for cities software](https://support.google.com/waze/partners/answer/9326703?hl=en&ref_topic=9326304)

## Running

Execute in the repository root: `docker-compose up`

Then, for the **first time only**, you'll need to setup the database with the following commands:

* `docker-compose run webserver python manage.py migrate`
* `docker-compose run webserver python manage.py collectstatic --no-input --clear`
* `docker-compose run -v $(pwd)/data:/app/data webserver python manage.py build_graph`

After everything finishes starting up, services will be available as follows:

* `localhost:8080` - São Paulo OTP instance
* `localhost:8081` - Xalapa OTP instance
* `localhost:8082` - Lima OTP instance
* `localhost:8083` - Montevideo OTP instance
* `localhost:8000` - Webserver
