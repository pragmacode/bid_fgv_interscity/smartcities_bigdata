\documentclass[paper=a4, fontsize=11pt]{scrartcl}
\usepackage[T1]{fontenc}
\usepackage{fourier}

\usepackage[brazil]{babel}                              % English language/hyphenation
\usepackage[protrusion=true,expansion=true]{microtype}
\usepackage{amsmath,amsfonts,amsthm} % Math packages
\usepackage[pdftex]{graphicx}
\usepackage[hyphens]{url}
\usepackage{hyperref}
\usepackage{tabu}

% Configure referencing styles
\hypersetup{
  colorlinks=true,
  linkcolor=blue,
  filecolor=magenta,
  urlcolor=blue
}
\urlstyle{same}

%%% Custom sectioning
\usepackage{sectsty}
\allsectionsfont{\centering \normalfont\scshape}


%%% Custom headers/footers (fancyhdr package)
\usepackage{fancyhdr}
\pagestyle{fancyplain}
\fancyhead{}                      % No page header
\fancyfoot[L]{}                      % Empty
\fancyfoot[C]{}                      % Empty
\fancyfoot[R]{\thepage}                  % Pagenumbering
\renewcommand{\headrulewidth}{0pt}      % Remove header underlines
\renewcommand{\footrulewidth}{0pt}        % Remove footer underlines
\setlength{\headheight}{13.6pt}


%%% Equation and float numbering
\numberwithin{equation}{section}    % Equationnumbering: section.eq#
\numberwithin{figure}{section}      % Figurenumbering: section.fig#
\numberwithin{table}{section}        % Tablenumbering: section.tab#


%%% Maketitle metadata
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}   % Horizontal rule

\title{
  %\vspace{-1in}
  \usefont{OT1}{bch}{b}{n}
  \huge Relatório Técnico {\textendash} Smartcities Bigdata\\
  \horrule{2pt} \\[0.5cm]
}
\author{
  \normalfont\normalsize
  \today
}
\date{}


%%% Begin document
\begin{document}

\maketitle

O Smartcities Bigdata é um conjunto de softwares para coleta de dados relacionados ao trânsito em cidades urbanas, especialmente sobre ônibus e dados do Waze\footnote{\url{https://www.waze.com/ccp/}}, com foco em provas de conceito que demonstrem às cidades exemplos de produtos que podem ser criados a partir dos dados fornecidos. Como cada cidade fornece informações distintas, os resultados são diferentes, além disso, outro ponto importante do sistema é demonstrar para elas o que é possível realizar com a disponibilização de mais informações.

As principais funcionalidades desenvolvidas são:

\begin{itemize}
  \item Aplicação web
    \begin{itemize}
      \item geração de grafo a partir do GTFS\footnote{\url{http://gtfs.org/}} de São Paulo (InterSCityBus)
      \item coleta e armazenamento de dados da API OlhoVivo\footnote{\url{https://sptrans.com.br/desenvolvedores/}} (InterSCityBus)
      \item coleta e armazenamento de dados do Waze
      \item interface unificada para geração de rotas de ônibus com Open Trip Planner\footnote{\url{https://www.opentripplanner.org/}} (OTP)
      \item instruções e automação dos passos para iniciar todos os softwares
    \end{itemize}
  \item Geração de arquivos GTFS compatíveis com OTP para São Paulo (Brasil), Xalapa (México), Lima (Peru) e Montevidéu (Uruguai)
\end{itemize}

Todo o código fonte está hospedado em \url{https://gitlab.com/pragmacode/bid_fgv_interscity/smartcities_bigdata} sob a licença de software livre Mozilla Public License 2.0.

\section{Arquitetura}

O código do sistema é estruturado em duas partes principais: um sistema web e um conjunto de scripts para geração de GTFS. Ambos são escritos na linguagem de programação Python\footnote{\url{https://www.python.org/}}, escolhida por sua popularidade. Na figura a seguir é possível ter uma visão geral da arquitetura e integração do sistema.

\includegraphics[width=\textwidth]{architecture}

A aplicação web foi criada utilizando o arcabouço Django\footnote{\url{https://www.djangoproject.com/}} para melhor organizar o código e deixá-lo padronizado para futuros desenvolvimentos.

Esse sistema tem três partes principais. A primeira, se integra à API do Waze para armazenar no MongoDB\footnote{\url{https://www.mongodb.com/}} dados de alertas, irregularidades e congestionamentos. Outra é a integração com o serviço OlhoVivo fornecido pela SPTRANS que também armazena no MongoDB os dados de geolocalização dos ônibus do município de São Paulo. Ambos os componentes persistem dados no MongoDB, escolhido também por sua popularidade, para que no futuro outras aplicações possam ser desenvolvidas para consumir esses dados.

A terceira parte do sistema tem a responsabilidade de prover uma interface web para que usuários possam planejar viagens de ônibus. Para possibilitar isso, foi utilizado o OTP e dados fornecidos pelas cidades sobre os itinerários dos ônibus. O OTP foi escolhido para fazer parte do sistema, pois tem o código aberto, já é utilizado com sucesso em outros projetos e provê, tanto uma interface web pronta para uso, quanto APIs que podem ser úteis para futuras implementações de visualizações avançadas.

Por fim, para utilizar o OTP é exigido que os dados de transporte público estejam formatados em GTFS e possui algumas limitações quanto a ele. Isso nos levou a criar scripts à parte da aplicação web que sejam capazes de ler os dados fornecidos pelas cidades em diferentes formatos e convertê-los para GTFS seguindo os requisitos para uso do OTP. A escolha por criar scripts fora da aplicação web se deu no contexto de buscar pela solução mais simples possível e com a visão de que os dados de ônibus das cidades não mudam com alta frequência.

  \subsection{Representação de GTFS em grafo}
  Como mencionado anteriormente, parte do Smartcities Bigdata consiste da integração do projeto InterSCityBus. Esse projeto a partir do GTFS estático da cidade de São Paulo constrói um grafo no qual posteriormente mapeia em tempo real os dados obtidos por meio da API OlhoVivo. O objetivo dessa forma de representar os dados é permitir que sejam criadas métricas (por exemplo, congestionamento em corredores de ônibus) e ferramentas (por exemplo, estimativa do tempo de chegada do próximo ônibus em um ponto) que sejam úteis para técnicos e para os usuários do sistema.

  Como parte da integração do InterSCityBus ao Smartcities Bigdata, tanto o grafo gerado quanto os dados coletados em tempo real foram modelados para armazenamento no MongoDB. Dessa forma, possibilitamos o acesso em paralelo ao mesmo conjunto de dados por diferentes aplicações, inclusive em contextos de computação distribuída.

  \subsection{Escalabilidade do OTP}
  A documentação do OTP\footnote{\url{https://github.com/opentripplanner/OpenTripPlanner/blob/77d24b64973ff2873f14c0cf374333c23031ac76/docs/Version-Comparison.md} e \url{https://github.com/opentripplanner/OpenTripPlanner/blob/dev-2.x/docs/History.md\#opentripplanner-2}} menciona que a versão 2, usada no projeto, foi criada tendo como um de seus objetivos a escalabilidade. Porém, não foram encontrados estudos e resultados que comprovem e que forneçam referências sobre como adequar os recursos computacionais às expectativas de demanda.

  Por isso e dado que a aplicação de roteirização tem potencial para ter alta demanda do público das cidades, recomendamos que essas realizem os testes apropriados após a instalação de forma a simular cenários pertinentes às suas realidades de demanda.

  \subsection{Pontos de atenção da arquitetura}\label{subsec:pontos_de_atencao}
  Devido ao curto tempo para desenvolvimento, o foco do projeto foi criar provas de conceito para as cidades sobre o que é possível realizar com os dados de trânsito e apontar quais informações são importantes para atingir os resultados. Por isso, pontos importantes relativos à qualidade do software tiveram menos importância. Outra parte dos resultados apresentados, por falta de dados ou por formatação incompatível com as ferramentas, se baseia em suposições e aproximações, pois também não seria possível esperar que as cidades produzissem os dados necessários a tempo do fim do projeto.

  Não foi possível aplicar todas as práticas de desenvolvimento que garantem um software de qualidade que pode ser utilizado em produção. A seguir são listados os pontos que precisam melhorar:

  \begin{itemize}
    \item incorporar uma ferramentas de checagem para
      \begin{itemize}
        \item duplicação de código
        \item versionamento indevido de valores secretos
        \item instalação de dependências com licenças incompatíveis com a do projeto
      \end{itemize}
    \item testes automatizados
      \begin{itemize}
        \item mensurar cobertura
        \item cobrir 100\% do código com testes de unidade
        \item criar testes de aceitação para todas as funcionalidades
      \end{itemize}
    \item criar esteira de integração contínua
    \item revisitar o código refatorando em classes coesas que fazem melhor uso de atributos e métodos com significado semântico
  \end{itemize}

\section{Instalação}

O repositório do sistema contém o arquivo ``HACKING.md'' que fornece instruções detalhadas para a operação do sistema. A seguir, serão repetidas as instruções para instalação utilizando Docker Compose, pois a consideramos a mais simples para usuários novos. Mesmo sendo mais simples, ainda é recomendável que elas sejam executadas por um profissional capacitado para operar sistemas de informação.

  \subsection{Requisitos}
  \begin{itemize}
    \item Python 3.9\footnote{\url{https://www.python.org/downloads/}}
    \item Poetry\footnote{\url{https://python-poetry.org/docs/\#installation}}
    \item Git\footnote{\url{https://git-scm.com/downloads}}
    \item Docker\footnote{\url{https://docs.docker.com/get-docker/}}
    \item Docker Compose\footnote{\url{https://docs.docker.com/compose/install/}}
  \end{itemize}

  \subsection{Configuração}
  O primeiro passo é, usando o Git, clonar o repositório do projeto em \url{https://gitlab.com/pragmacode/bid_fgv_interscity/smartcities_bigdata}. Dentro da pasta criada, que chamaremos de raiz do projeto, é preciso instalar as dependências do projeto executando:

  \vspace{1em}
  \texttt{poetry install}
  \vspace{1em}

  Que fará o download dos pacotes necessários e suas dependências. A título de completude, listamos aqui os pacotes base
  que usamos:

  \begin{itemize}
    \item \texttt{behave-django}
    \item \texttt{chromedriver} 
    \item \texttt{django} 
    \item \texttt{django-bootstrap4} 
    \item \texttt{django-selenium} 
    \item \texttt{djongo} 
    \item \texttt{geopandas} 
    \item \texttt{geopy} 
    \item \texttt{networkx} 
    \item \texttt{pandas} 
    \item \texttt{pykml} 
    \item \texttt{pylama} 
    \item \texttt{pyproj} 
    \item \texttt{pytest-django} 
    \item \texttt{pytest-mock} 
    \item \texttt{requests} 
    \item \texttt{selenium} 
    \item \texttt{sentry-sdk} 
    \item \texttt{shapely} 
  \end{itemize}

  Feito isso, será preciso criar a seguinte estrutura de pastas com os dados arquivos que serão utilizados de base para o sistema:

  \begin{itemize}
    \item \texttt{data/}
      \begin{itemize}
        \item \texttt{sao\_paulo/}
          \begin{itemize}
            \item \texttt{otp/}
              \begin{itemize}
                \item \texttt{buslines.gtfs.zip}
                \item \texttt{map.pbf}
              \end{itemize}
            \item \texttt{gtfs/}
          \end{itemize}
        \item \texttt{xalapa/}
          \begin{itemize}
            \item \texttt{otp/}
              \begin{itemize}
                \item \texttt{buslines.gtfs.zip}
                \item \texttt{map.pbf}
              \end{itemize}
          \end{itemize}
        \item \texttt{lima/}
          \begin{itemize}
            \item \texttt{otp/}
              \begin{itemize}
                \item \texttt{buslines.gtfs.zip}
                \item \texttt{map.pbf}
              \end{itemize}
          \end{itemize}
        \item \texttt{montevideo/}
          \begin{itemize}
            \item \texttt{otp/}
              \begin{itemize}
                \item \texttt{buslines.gtfs.zip}
                \item \texttt{map.pbf}
              \end{itemize}
          \end{itemize}
        \item \texttt{mongodb/}
      \end{itemize}
  \end{itemize}

  Nessa estrutura de pastas, os arquivos \texttt{map.pbf} são extrações do Open Street Map para cada uma das cidades. Já a pasta \texttt{mongodb/} inicialmente é um diretório vazio que posteriormente será utilizado pela aplicação para persistir dados. A seguir, para cada cidade, será detalhado o que é esperado nos demais subdiretórios e arquivos.

  Na pasta \texttt{sao\_paulo/}, estão os dados da cidade de \textbf{São Paulo}. No subdiretório \texttt{gtfs/}, devem estar os arquivos não compactados do GTFS da cidade. No subdiretório \texttt{otp/}, o arquivo \texttt{buslines.gtfs.zip} contém praticamente o mesmo conteúdo do subdiretório \texttt{gtfs/}, porém os arquivos \texttt{stop\_times.txt} e \texttt{trips.txt} precisam ser ajustados seguindo os passos explicitados na seção~\ref{sec:static_gtfs}.

Os dados de \textbf{Xalapa} devem estar no diretório \texttt{xalapa/}. Como não foi fornecido um GTFS oficial para ela, criamos um script que o gera a partir dos dados produzidos no \textit{Mapatón Xalapa 2016}. A partir da raiz do projeto, há o arquivo \texttt{samples/gtfs/xalapa.gtfs.zip} que deixamos já gerado para facilitar esse processo. Você pode copiá-lo para dentro do subdiretório \texttt{otp/}.

  Os dados de \textbf{Lima} devem estar no diretório \texttt{lima/}. Como não foi fornecido um GTFS oficial para ela, criamos um script que o gera a partir dos dados fornecidos pelo contato da cidade. A partir da raiz do projeto, há o arquivo \texttt{samples/gtfs/lima.gtfs.zip} que deixamos já gerado para facilitar esse processo. Você pode copiá-lo para dentro do subdiretório \texttt{otp/}.

  Os dados de \textbf{Montevidéo} devem estar no diretório \texttt{montevideo/}. Como não foi fornecido um GTFS oficial para ela, criamos um script que o gera a partir dos dados disponíveis em \url{https://catalogodatos.gub.uy/organization/d9026405-35be-410e-b251-492fba99c57d?groups=transporte}. A partir da raiz do projeto, há o arquivo \texttt{samples/gtfs/montevideo.gtfs.zip} que deixamos já gerado para facilitar esse processo. Você pode copiá-lo para dentro do subdiretório \texttt{otp/}.

  Por último, edite o arquivo \texttt{docker-compose.yml} presente na raiz do projeto.

  \begin{itemize}
    \item \texttt{SECRET\_KEY}: chave aleatória secreta cuja função é validar requisições e que você deve gerar como preferir
    \item \texttt{OLHO\_VIVO\_TOKEN}: obtido no portal para desenvolvedores da SPTRANS\footnote{\url{https://sptrans.com.br/desenvolvedores/}}
    \item \texttt{SENTRY\_DSN}: chave do serviço de notificação de erros que deve ser obtido por meio da criação de uma conta\footnote{\url{https://sentry.io}}
    \item \texttt{WAZE\_BASE\_URL}: obtida por meio do cadastro no programa da Waze para cidades\footnote{\url{https://support.google.com/waze/partners/answer/9326703?hl=en&ref_topic=9326304}}
  \end{itemize}

  \subsection{Execução}
  A partir da raiz do projeto, execute:

  \vspace{1em}
  \texttt{docker-compose up}
  \vspace{1em}

  Com esse comando, todos os processos necessários devem ser iniciados. Os serviços estarão disponíveis nas seguintes URLs:

  \begin{itemize}
    \item \url{localhost:8080} - São Paulo OTP instance
    \item \url{localhost:8081} - Xalapa OTP instance
    \item \url{localhost:8082} - Lima OTP instance
    \item \url{localhost:8083} - Montevideo OTP instance
    \item \url{localhost:8000} - Webserver
  \end{itemize}

\section{Instruções de uso}

  \subsection{Roteirização}
  Para iniciar a operação do sistema acesse \url{http://localhost:8000/otp_interface/sao_paulo}.

  \vspace{1em}
  \includegraphics[width=\textwidth]{routing_1}
  \vspace{1em}

  No canto direito superior existe o menu ``Routing'' que permitirá escolher a cidade para a qual deseja gerar rotas.

  \vspace{1em}
  \begin{center}
  \includegraphics[width=.3\textwidth]{routing_3}
  \end{center}
  \vspace{1em}

  Para calcular um rota, clique no mapa uma vez para especificar o ponto de partida e uma segunda vez para o destino. Após o segundo clique serão exibidas todas as opções de rota encontradas.

  \vspace{1em}
  \includegraphics[width=0.95\textwidth]{routing_2}
  \vspace{1em}

  \subsection{Acessando dados armazenados}
  Para adicionar uma nova cidade para ter os dados da API do Waze coletados:

  \begin{enumerate}
    \item edite \texttt{smartcities\_bigdata/settings.py}
    \item adicione uma nova entrada no dicionário \texttt{WAZE\_POLYGONS} com os dados da cidade
  \end{enumerate}

  Para se conectar ao MongoDB e consumir os dados que são coletados do Waze e OlhoVivo faça:

    \vspace{1em}
    \texttt{docker-compose run mongo mongo}
    \vspace{1em}

  Esse comando deve abrir o CLI do MongoDB para que você possa realizar consulta. Para informações sobre a estrutura de armazenamento, cheque os arquivos do projeto \texttt{interscitybus/models.py} e \texttt{waze/models.py}.

\section{Geração de GTFS estático}\label{sec:static_gtfs}

O \textit{General Transit Feed Specification} (GTFS) é um especificação de dados que permite agências públicas de trânsito publicar seus dados de trânsito em um formato que pode ser consumido por uma vasta gama de softwares. Ele é composto por duas partes: a estática, que contem dados sobre agenda, tarifa e componentes geográficos; e a em tempo real que contém previsões de chegada, posição dos veículos e avisos sobre os serviços.

Nesse projeto, o foco foi a geração da parte estática com base em informações fornecidas pelas cidades a respeito do transporte público com ônibus. O objetivo final é produzir GTFS compatíveis com o OTP.

A cidade de \textbf{São Paulo} já disponibiliza um GTFS atualizado através do portal da SPTRANS para desenvolvedores, porém esse vem num formato não suportado pelo OTP\footnote{\url{https://github.com/opentripplanner/OpenTripPlanner/issues/3262}}. Para ajustá-lo de acordo com os requisitos do OTP foi criado um script que pode ser utilizado seguindo os passos:

\begin{enumerate}
  \item certifique-se que no GTFS de São Paulo existe um arquivo \texttt{frequencies.txt} e de que ele esteja no diretório correto
  \item a partir da raiz do projeto, entre no diretório \texttt{gtfs\_generators}
  \item execute \texttt{poetry run python sp\_use\_frequencies.py}
  \item isso gerará arquivos \texttt{new\_stop\_times.txt} e \texttt{new\_trips.txt}
  \item esses arquivos gerados devem ser copiados dentro do \texttt{.gtfs.zip}, no lugar dos \texttt{trips.txt} and  \texttt{stop\_times.txt} originais (lembre-se de remover o prefixo \texttt{new\_})
  \item por último remova o arquivo \texttt{frequencies.txt}
\end{enumerate}

\textbf{Xalapa} não possui um GTFS estático pronto, mas há informações sobre as rotas de ônibus em \url{https://datos.gob.mx/busca/dataset/rutas-tranporte-publico}. Esses dados estão no formato GeoJSON\footnote{\url{https://datatracker.ietf.org/doc/html/rfc7946}} e foi criado um script para gerar GTFS a partir deles que pode ser usado da seguinte forma:

  \vspace{1em}
  \texttt{poetry run python gtfs\_generators/esri\_shapefile\_to\_gtfs.py <ROOT DIR>}
  \vspace{1em}

Onde \texttt{<ROOT\_DIR>} é a pasta contendo os dados ESRI Shapefile.

Recebemos diretamente dos representantes de \textbf{Miraflores} um arquivo no formato KML\footnote{\url{https://developers.google.com/kml/documentation}} contendo todas as rotas de Lima. Para este também foi criado um script que gera o GTFS. Para executá-lo faça:

  \vspace{1em}
  \texttt{poetry run python gtfs\_generators/kml\_to\_gtfs.py <ROOT\_DIR>}
  \vspace{1em}

Onde \texttt{<ROOT\_DIR>} é a pasta contendo os dados KML.

Para \textbf{Montevidéu} foram encontrados dados no endereço \url{https://catalogodatos.gub.uy/organization/d9026405-35be-410e-b251-492fba99c57d?groups=transporte}. Nesse, faça download dos seguintes arquivos:

\begin{itemize}
  \item \texttt{v\_uptu\_lsv\_destinos.zip}
  \item \texttt{uptu\_variante\_no\_maximal.zip}
  \item \texttt{v\_uptu\_variantes\_circulares.zip}
  \item \texttt{v\_uptu\_paradas.zip}
  \item \texttt{HORARIOS\_OMNIBUS datos.zip}
  \item \texttt{uptu\_pasada\_variante.zip}
  \item \texttt{uptu\_pasada\_circular.zip}
\end{itemize}

Depois execute o script:

  \vspace{1em}
  \texttt{poetry run python gtfs\_generators/montevideo\_data\_to\_gtfs.py <ROOT\_DIR>}
  \vspace{1em}

Onde \texttt{<ROOT\_DIR>} é a pasta contendo os arquivos listados acima.

  \subsection{Geração de GTFS para outras cidades}
  Com os scripts desenvolvidos, podemos gerar GTFS para outras cidades que forneçam seus dados no formato \textbf{ESRI
  Shapefile} ou \textbf{KMZ}.

  \begin{itemize}
    \item ESRI Shapefile:

      Crie um diretório que contenha uma pasta para cada linha de ônibus, dentro delas, é obrigatório ter um arquivo
      chamado \texttt{route.zip}, que contenha um shapefile com a rota num estrutura de Linestring; e pode também ter um
      arquivo opcional chamado \texttt{stops.zip}, que contenha um shapefile com as paradas da rota, fornecidas em
      estrutura de lista.

      \vspace{1em}
      \texttt{poetry run python gtfs\_generators/esri\_shapefile\_to\_gtfs.py <ROOT DIR>}
      \vspace{1em}

      Onde \texttt{<ROOT\_DIR>} é a pasta contendo os dados ESRI Shapefile.

    \item KMZ:

      Basta extrair o arquivo \texttt{.kml} de dentro do \texttt{.kmz} que contem as rotas de ônibus e rodar o mesmo
      script que usamos para a cidade de Lima, apontando para a localização do arquivo .kml, isto é:

      \vspace{1em}
      \texttt{poetry run python gtfs\_generators/kml\_to\_gtfs.py <ROOT\_DIR>}
      \vspace{1em}

      Onde \texttt{<ROOT\_DIR>} é a pasta contendo o arquivo .kml extraído.
  \end{itemize}

  \subsection{Sugestões às cidades}
  O formato GTFS tem uma especificação relativamente aberta, permitindo que os mesmos dados possam ser representados de formas distintas. Aplicações que usam esse arquivo, como o OTP, geralmente não suportam todas essas variações e definem um subconjunto suportado.

  Nesse contexto, a sugestão para \textbf{São Paulo}, é para gerar o GTFS sem usar o \texttt{frequencies.txt} como forma de oferecer uma versão de maior compatibilidade com as aplicações que consomem GTFS. Entendemos que a versão atual é interessante do ponto de vista de ser mais enxuta, portanto uma alternativa seria disponibilizar para download as duas alternativas.

  \textbf{Montevidéu} não fornece o GTFS diretamente, mas disponibiliza todas as informações necessárias para gerá-lo exigindo poucas suposições por parte do programador. Essa quantidade grande de dados trouxe desafios com respeito à organização, consistência e acurácia dos dicionários de dados conforme listados a seguir:
  \begin{itemize}
    \item a documentação lista 3 tipos de serviço (dias úteis, sábados e domingos), porém havia um quarto tipo nos dados para os quais convencionamos que representam linhas que operam todos os dias;
    \item alguns arquivos tem informações conflitantes entre si, por exemplo identificadores repetidos de linhas diferentes, casos nos quais adicionamos sufixos para diferenciar as linhas.
  \end{itemize}

  Já para \textbf{Xalapa} e \textbf{Lima} havia menos informação disponível do que o necessário para um GTFS minimal o que nos forçou, para gerar um GTFS, a fazer suposições mais fortes:
  \begin{itemize}
    \item os arquivos \texttt{calendar.txt} e \texttt{agency.txt} foram gerados com informações fictícias
    \item linhas sem informações sobre paradas tiveram paradas criadas a cada 500m;
    \item para todas as linhas foi suposto que a primeira viagem parte às 6h00 e a última às 21h00, todos os dias;
    \item viagens partem a cada 15 minutos;
    \item o tempo de viagem entre paradas é fixo em 2 minutos.
  \end{itemize}

  Portanto, para gerar GTFS mais precisos para essas duas cidades os seguintes dados são importantes:
  \begin{itemize}
    \item dias de operação e as linhas categorizadas de acordo;
    \item informações das empresas que operam o sistema e as linhas categorizadas de acordo;
    \item informações completas sobre todas as paradas de ônibus e quais linhas passam por elas em cada horário específico;
    \item horário de partida de cada viagem de cada linha;
    \item tempo gasto para o veículo percorrer o trajeto entre cada parada.
  \end{itemize}

\section{Próximos passos}

O projeto apresentado é suficiente para demonstrar aos gestores das cidades possíveis aplicações que podem ser produzidas com dados públicos sobre transporte, porém para ser utilizado pelo público geral entendemos que são necessárias evoluções.

Como primeiro passo sugerimos investir na qualidade geral do sistema atendendo ao apontamentos feitos previamente nesse relatório na seção ``Pontos de atenção da arquitetura''. Feito isso, a seguir são destacadas as questões relevantes para roteirização, geração de GTFS e processo de instalação.

  \subsection{Roteirização}
    Conforme os cidadãos utilizarem a plataforma outras demandas surgirão, mas no momento que escrevemos esse relatório as que nos parecem mais relevantes são:

    \begin{enumerate}
      \item criar uma interface gráfica mais simples do que a fornecida pelo OTP,
        \begin{itemize}
          \item dado que a interface usada atualmente é parte do OTP e a utilizamos no projeto para atenderas demandas dentro do prazo,
          \item contudo, há o entendimento de que ela é mais complexa do que o necessário para o público geral e pode trazer dificuldades no uso,
          \item outro ponto que uma nova interface gráfica precisará melhorar com relação à do OTP é quanto à internacionalização, visto que em nosso estudo não encontramos formas de traduzi-la para outros idiomas;
        \end{itemize}
      \item criar interface gráfica para o gestor fazer upload de dados GTFS e do OSM atualizados;
      \item quantificar a demanda esperada do público em acessos por segundo;
      \item realizar testes de escalabilidade sobre o OTP para definir os requisitos de recursos computacionais necessários e provisioná-los de acordo com a demanda esperada;
    \end{enumerate}

  \subsection{Geração de GTFS}
    O principal foco é facilitar o uso pelas cidades sempre que houverem dados mais recentes de forma que essas possam atualizar sem precisar de auxílio de uma equipe técnica:
    \begin{enumerate}
      \item melhorar os scripts para tratar erros de formato e exibir mensagens significativas que permitam diagnostico e correção mais rápidos;
      \item melhorar os scripts definindo um formato de entrada que as cidades possam utilizar para fornecer os dados faltantes mencionados anteriormente de forma que o GTFS gerado seja mais completo e sem dados fictícios;
      \item criar uma interface gráfica que permita ao gestor público fazer o upload dos dados brutos da cidade para processamento e depois o download do GTFS.
    \end{enumerate}

  \subsection{Instalação}
    O processo de instalação das ferramentas descrito no relatório é dependente de técnicos capacitados para realizá-lo. Essa dependência foi necessária para podermos entregar processos agnósticos em termos de provedor de infraestrutura e sistema operacional. Ou seja, garantir que os sistemas possam ser instalados pelo maior número de cidades.

    Por outro lado, nem todas as cidades podem ter técnicos capacitados disponíveis, mas podem ter recursos para contratar serviços de infraestrutura terceirizados. Nesse contexto, é interessante ter também os processos de instalação preparados para pelo menos um grande provedor de infraestrutura, como a AWS. Dessa forma, o gestor poderá seguir instruções para contratar os serviços desse provedor e instalar a plataforma. Feito isso, os próprios serviços do provedor devem se assegurar automaticamente quanto à escalabilidade e disponibilidade eliminando grande parte da dependência de técnicos especializados para operar a plataforma.

\end{document}
