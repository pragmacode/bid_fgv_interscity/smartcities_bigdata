In `main.tex` you'll find the first version of our technical report having in mind it was a product of its own.

In `main_fgv_model.tex` there's a version of the report with the content adapted to later be merged into a bigger document that later FGV will deliver to BID.

## Compilation:

* `latexmk -pdf main.tex`
* `latexmk -pdf main_fgv_model.tex`
* `latexmk -pdf presentation.tex`
