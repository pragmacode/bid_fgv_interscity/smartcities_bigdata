# HACKING Smartcities BigData

## Conventions

* The project idiom for code, reference files, Gitlab content and any other content is English
* Create atomic commits
  - by introducing smaller and self-contained changes we make easier for other developers to understand the code by studying the commits
  - reference text: https://coderwall.com/p/jmqp0a/why-and-how-i-craft-atomic-commits-in-git
* Write meaningful commit messages
  - focus on why instead of what
  - reference text: https://chris.beams.io/posts/git-commit/
* Create tests that fail without your code and pass with it

## Directory structure overview

Here, a short overview of project folders:

* bin
  - executable files, including Docker entrypoint
* deploy
  - contains deploy instructions (in the future it may hold infrastructure automation code)
* features
  - folder which will contain the acceptance tests and its configuration
* gtfs_generators
  - contains the scripts that generate GTFS sets from other formats
* interscitybus
  - contains the adaptation to Django app from the original InterSCityBus framework which translates SP GTFS into graphs and maps OlhoVivo data into it
* locale
  - stores locale settings (en, es and pt)
* nginx
  - contains the configuration file for the NGINX instance we are bringing up with Docker Compose
* otp
  - folder where we have the necessary files to run an OTP instance
* otp_interface
  - Django app with views rendering our OTP instance where we can use all the cities
* samples
  - pre-made GTFS samples
* shared_templates
  - contains Django templates that will be shared by more than one app
* smartcities_bigdata
  - Django project general settings
* static_pages
  - Django app that stores views that doesn't have dynamic content
* technical_report
  - LaTeX source files for the technical report
* waze
  - Django application that retrieves real time data from waze

## Development environment configuration

### System dependencies

* Python 3.9
* [Poetry](https://python-poetry.org/docs/#installation)
* [MongoDB](https://www.mongodb.com/download-center/community/releases)
* Java

### Application configuration

* Python dependencies installation
  - `poetry install`
* Set your local configurations
  - `cp smartcities_bigdata/local_settings.py.sample smartcities_bigdata/local_settings.py`
  - edit the file with your own information `smartcities_bigdata/local_settings.py`
* Database setup
  - `poetry run ./manage.py migrate`
* OTP setup
  - place inside the `otp/data` directory the zipped GTFS
    * its name is expected to end in `.gtfs.zip`
    * check [OTP finds no routes](#otp-finds-no-routes), specially if you want to work with São Paulo GTFS
  - place OSM data inside `otp/data` directory
    * common sources are [Interline](https://www.interline.io/osm/extracts/) and [Geofabrik](https://download.geofabrik.de/)

### Running the application

* Django app `poetry run ./manage.py runserver`
* OTP `cd otp && make`

### Running tests

* Lint
  - `poetry run pylama` 
* Unit tests
  - `poetry run pytest`
* Acceptance tests
  - `poetry run ./manage.py behave`

## Feature life cycle from conception to client review

1. Create an issue
  - make sure it contains information about the requester, motivation and technical tasks list
2. In the weekly iteration review meetings, prioritize the issue
3. Supposing the issue was prioritized to the upcoming iteration, it should be moved to the Board ToDo column (adding the ToDo label)
4. Supposing you are going to work on this issue, assign yourself to it and move it to the Board Doing column
5. Create a branch starting from master where you'll create your commits
6. Work with the code, create commits and push them
7. Create a Merge Request and wait for review
8. If the reviewer ask you questions or ask for changes, repeat step 6 until approval
9. With the reviewer approval your code should be merged to the master branch, move the issue to the Board Done column
10. In the next weekly iteration review meeting present your work
  - if no changes are required, close the issue
  - otherwise, go back to step 4

## Code overview

TODO: describe the main classes and files so we can help fitire developers getting started

## Backup

### Requirements

* mongodump
* mongorestore

Read more about MongoDB backup tools at: https://docs.mongodb.com/manual/tutorial/backup-and-restore-tools/.

### Create

`mongodump -o <BACKUP DIR>`

### Restore

`mongorestore <BACKUP DIR>`

## Troubleshooting

### OTP finds no routes

The current OTP version [does not support creating trips from frequencies](https://github.com/opentripplanner/OpenTripPlanner/issues/3262).
Thus, GTFS `stop_times.txt` must contain the timestamps for all trips, so we made a script to gather the data from the
`frequencies.txt` file and generate both `new_trips.txt` and `new_stop_times.txt` files.

We also, use the information regarding the time interval between each stop, from the original `stop_times.txt` file,
assuming that this interval is the same for all trips.

Under the `gtfs_generators` directory you'll find the `sp_use_frequencies.py` script. In order to use it:

* enter `gtfs_generators` dir
* run `poetry run python sp_use_frequencies.py <PATH>`, where `<PATH>` is the folder containing São Paulo GTFS files.
* it will generate `new_stop_times.txt` and `new_trips.txt` files.
* copy these files inside you `.gtfs.zip`, remove the original `trips.txt` and `stop_times.txt` from the zip, and remove
    the `new_` prefix from the files you've just copied inside the zip.
* if there is a `frequencies.txt` inside the zip, also remove it.

Now all should be good to rebuild the OTP graph.
