from django.conf import settings
from sentry_sdk import capture_exception

import requests


class Api:
    def get(self):
        responses = {}
        for poly_id, details in settings.WAZE_POLYGONS.items():
            print(poly_id)

            try:
                response = requests.get(settings.WAZE_BASE_URL + details['points'])
                responses[poly_id] = response.json()
            except Exception as e:
                responses[poly_id] = None
                capture_exception(e)
                print(e)

        return responses
