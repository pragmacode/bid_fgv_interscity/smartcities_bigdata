from django.core.management.base import BaseCommand

import time

from waze.api import Api
from waze.models import Alert, Irregularity, Jam


class Command(BaseCommand):
    help = 'Collect data from Waze APIs'

    def handle(self, *args, **options):
        print('Collecting waze')
        api = Api()

        while True:
            polygons_json_lists = api.get()

            for poly_id, polygons_json_list in polygons_json_lists.items():
                if polygons_json_list.get('alerts'):
                    Alert.create_from_json_list(polygons_json_list.get('alerts'), poly_id)

                if polygons_json_list.get('irregularities'):
                    Irregularity.create_from_json_list(polygons_json_list.get('irregularities'), poly_id)

                if polygons_json_list.get('jams'):
                    Jam.create_from_json_list(polygons_json_list.get('jams'), poly_id)

            time.sleep(40)
