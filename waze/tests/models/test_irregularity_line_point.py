import pytest

# pylama:ignore=W0611
from waze.models import IrregularityLinePoint


@pytest.mark.skip(reason='The feature is due to 2021-04-05 and I think there will be no time to test it now')
def test_attributes():
    pass
