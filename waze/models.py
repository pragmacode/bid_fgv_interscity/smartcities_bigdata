from django.db import models
from datetime import datetime
from pytz import UTC


class Alert(models.Model):
    waze_uuid = models.UUIDField()
    polygon_slug = models.CharField(null=False, max_length=100, blank=False)
    reported_at = models.DateTimeField()  # originally named pub_utc_date
    country = models.CharField(max_length=100)
    city = models.CharField(null=False, max_length=100, blank=False)
    street = models.CharField(max_length=100)
    road_type = models.IntegerField()
    magvar = models.IntegerField()
    lat = models.FloatField()
    lon = models.FloatField()
    type = models.CharField(max_length=100)
    subtype = models.CharField(max_length=100)
    n_thumbs_up = models.IntegerField()
    reliability = models.IntegerField()
    report_rating = models.IntegerField()
    confidence = models.IntegerField()
    report_description = models.TextField()
    irregularity = models.ForeignKey('waze.Irregularity', on_delete=models.CASCADE, blank=True, null=True)

    @classmethod
    def build_from_json(cls, alert_json, polygon_slug, irregularity=None):
        reported_at = None
        if alert_json.get('pubMillis', None):
            reported_at = datetime.fromtimestamp(alert_json['pubMillis']/1000.0).replace(tzinfo=UTC)

        return cls(
            waze_uuid=alert_json.get('uuid'),
            polygon_slug=polygon_slug,
            reported_at=reported_at,  # originally named pub_utc_date
            country=alert_json.get('country'),
            city=alert_json.get('city'),
            street=alert_json.get('street'),
            road_type=alert_json.get('roadType'),
            magvar=alert_json.get('magvar'),
            lat=alert_json.get('location', {'x': None})['x'],
            lon=alert_json.get('location', {'y': None})['x'],
            type=alert_json.get('type'),
            subtype=alert_json.get('subtype'),
            n_thumbs_up=alert_json.get('nThumbsUp'),
            reliability=alert_json.get('reliability'),
            report_rating=alert_json.get('reportRating'),
            confidence=alert_json.get('confidence'),
            report_description=alert_json.get('reportDescription'),
            irregularity=irregularity,
        )

    @classmethod
    def create_from_json_list(cls, json_list, polygon_slug, irregularity=None):
        alerts = []

        for alert_json in json_list:
            alerts.append(cls.build_from_json(alert_json, polygon_slug, irregularity))

        return cls.objects.bulk_create(alerts)


class Irregularity(models.Model):
    waze_id = models.IntegerField()
    polygon_slug = models.CharField(null=False, max_length=100, blank=False)
    reported_at = models.DateTimeField()  # originally named pub_utc_date
    update_date = models.DateTimeField()
    detection_date = models.DateTimeField()
    country = models.CharField(max_length=100)
    city = models.CharField(null=False, max_length=100, blank=False)
    street = models.CharField(max_length=100)
    length = models.IntegerField()
    highway = models.BooleanField()
    start_node = models.CharField(max_length=100)
    end_node = models.CharField(max_length=100)
    cause_type = models.CharField(max_length=100)
    cause_alert = models.ForeignKey(
        Alert, on_delete=models.CASCADE, blank=True, null=True, related_name='caused_irregularity'
    )
    speed = models.FloatField()
    trend = models.IntegerField()
    seconds = models.IntegerField()
    delay_seconds = models.IntegerField()
    severity = models.IntegerField()
    regular_speed = models.FloatField()
    jam_level = models.IntegerField()
    type = models.CharField(max_length=100)
    alerts_count = models.IntegerField()
    drivers_count = models.IntegerField()
    n_images = models.IntegerField()
    n_comments = models.IntegerField()
    n_thumbs_up = models.IntegerField()

    @classmethod
    def create_from_json_list(cls, json_list, polygon_slug):
        irregularities = []

        for irreg_json in json_list:
            reported_at = None
            if irreg_json.get('detectionDateMillis', None):
                reported_at = datetime.fromtimestamp(irreg_json['detectionDateMillis']/1000.0).replace(tzinfo=UTC)

            update_date = None
            if irreg_json.get('updateDateMillis', None):
                update_date = datetime.fromtimestamp(irreg_json['updateDateMillis']/1000.0).replace(tzinfo=UTC)

            irregularity = cls.objects.create(
                waze_id=irreg_json.get('id'),
                polygon_slug=polygon_slug,
                reported_at=reported_at,
                update_date=update_date,
                detection_date=reported_at,
                country=irreg_json.get('country'),
                city=irreg_json.get('city'),
                street=irreg_json.get('street'),
                length=irreg_json.get('length'),
                highway=irreg_json.get('highway'),
                start_node=irreg_json.get('startNode'),
                end_node=irreg_json.get('endNode'),
                cause_type=irreg_json.get('causeType'),
                speed=irreg_json.get('speed'),
                trend=irreg_json.get('trend'),
                seconds=irreg_json.get('seconds'),
                delay_seconds=irreg_json.get('delaySeconds'),
                severity=irreg_json.get('severity'),
                regular_speed=irreg_json.get('regularSpeed'),
                jam_level=irreg_json.get('jamLevel'),
                type=irreg_json.get('type'),
                alerts_count=irreg_json.get('alertsCount'),
                drivers_count=irreg_json.get('driversCount'),
                n_images=irreg_json.get('nImages'),
                n_comments=irreg_json.get('nComments'),
                n_thumbs_up=irreg_json.get('nThumbsUp'),
            )

            if irreg_json.get('alerts') and len(irreg_json.get('alerts')) > 0:
                Alert.create_from_json_list(irreg_json.get('alerts'), polygon_slug, irregularity)

            if irreg_json.get('causeAlert'):
                cause_alert = Alert.build_from_json(irreg_json.get('causeAlert'), polygon_slug, irregularity)
                cause_alert.save()
                irregularity.cause_alert = cause_alert
                irregularity.save()

            if irreg_json.get('line'):
                for point in irreg_json.get('line'):
                    IrregularityLinePoint.objects.create(irregularity=irregularity, lat=point['x'], lon=point['y'])

            irregularities.append(irregularity)

        return irregularities


class IrregularityLinePoint(models.Model):
    irregularity = models.ForeignKey(Irregularity, on_delete=models.CASCADE)
    lat = models.FloatField()
    lon = models.FloatField()


class Jam(models.Model):
    waze_uuid = models.UUIDField()
    blocking_alert_uuid = models.UUIDField()
    polygon_slug = models.CharField(null=False, max_length=100, blank=False)
    reported_at = models.DateTimeField()  # originally named pub_utc_date
    country = models.CharField(max_length=100)
    city = models.CharField(null=False, max_length=100, blank=False)
    street = models.CharField(max_length=100)
    length = models.IntegerField()
    road_type = models.IntegerField()
    start_node = models.CharField(max_length=100)
    end_node = models.CharField(max_length=100)
    turn_type = models.CharField(max_length=100)
    level = models.IntegerField()
    speed_kmh = models.FloatField()
    delay = models.IntegerField()
    type = models.CharField(max_length=100)

    @classmethod
    def create_from_json_list(cls, json_list, polygon_slug):
        jams = []

        for jam_json in json_list:
            reported_at = None
            if jam_json.get('pubMillis', None):
                reported_at = datetime.fromtimestamp(jam_json['pubMillis']/1000.0).replace(tzinfo=UTC)

            jam = cls.objects.create(
                waze_uuid=jam_json.get('uuid'),
                blocking_alert_uuid=jam_json.get('blockingAlertUuid'),
                polygon_slug=polygon_slug,
                reported_at=reported_at,
                country=jam_json.get('country'),
                city=jam_json.get('city'),
                street=jam_json.get('street'),
                length=jam_json.get('length'),
                road_type=jam_json.get('roadType'),
                start_node=jam_json.get('startNode'),
                end_node=jam_json.get('endNode'),
                turn_type=jam_json.get('turnType'),
                level=jam_json.get('level'),
                speed_kmh=jam_json.get('speedKMH'),
                delay=jam_json.get('delay'),
                type=jam_json.get('type'),
            )

            if jam_json.get('line'):
                for point in jam_json.get('line'):
                    JamLinePoint.objects.create(jam=jam, lat=point['x'], lon=point['y'])

            jams.append(jam)

        return jams


class JamLinePoint(models.Model):
    jam = models.ForeignKey(Jam, on_delete=models.CASCADE)
    lat = models.FloatField()
    lon = models.FloatField()
