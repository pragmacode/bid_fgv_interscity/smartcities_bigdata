import pandas as pd
import datetime as dt

stop_times = pd.read_csv(
    'stop_times.txt',
    parse_dates=['arrival_time', 'departure_time'],
    date_parser=lambda x: dt.datetime.strptime(x, "%H:%M:%S")
)

trip_ids = pd.unique(stop_times['trip_id'])

trips = pd.read_csv('trips.txt')

ordered_stop_times = stop_times.sort_values(['trip_id', 'stop_sequence'])
ordered_stop_times['travel_time'] = ordered_stop_times['arrival_time'] - ordered_stop_times['departure_time'].shift(1)

columns = ['trip_id', 'arrival_time', 'departure_time', 'stop_id', 'stop_sequence']

fixed_stop_times_rows = []
fixed_trips_rows = []
fixed_frequencies_rows = []
for trip_id in trip_ids:
    print(trip_id, dt.datetime.now())

    trip = trips[trips['trip_id'] == trip_id].iloc[0]

    trip_stop_travel_time = ordered_stop_times \
        .loc[
            (ordered_stop_times['trip_id'] == trip_id)
            & ~ordered_stop_times['travel_time'].isna()
            & (ordered_stop_times['travel_time'] > dt.timedelta(0)),
            'travel_time'
        ] \
        .iloc[0]

    first_departure = dt.datetime.now().replace(hour=6, minute=0, second=0, microsecond=0)
    last_departure = dt.datetime.now().replace(hour=21, minute=59, second=59, microsecond=0)
    time = first_departure

    trip_stops = ordered_stop_times.loc[ordered_stop_times['trip_id'] == trip_id, ['stop_id', 'stop_sequence']]

    trip_counter = 0

    while time <= last_departure:
        arrival_time = time
        new_trip_id = trip_id+'-'+str(trip_counter)

        for _, trip_stop in trip_stops.iterrows():
            fixed_stop_times_rows.append(
                {
                    'trip_id': new_trip_id,
                    'arrival_time': arrival_time,
                    'departure_time': arrival_time,
                    'stop_id': trip_stop['stop_id'],
                    'stop_sequence': trip_stop['stop_sequence'],
                }
            )

            arrival_time += trip_stop_travel_time

        fixed_trips_rows.append(
            {
                'route_id': trip['route_id'],
                'service_id': trip['service_id'],
                'trip_id': new_trip_id,
                'trip_headsign': trip['trip_headsign'],
                'direction_id': trip['direction_id'],
                'shape_id': trip['shape_id'],
            }
        )

        fixed_frequencies_rows.append(
            {
                'trip_id': new_trip_id,
                'start_time': first_departure,
                'end_time': last_departure,
                'headway_secs': 600,
            }
        )

        time += dt.timedelta(minutes=15)
        trip_counter += 1

fixed_stop_times = pd.DataFrame(fixed_stop_times_rows)
fixed_stop_times['arrival_time'] = fixed_stop_times['arrival_time'].dt.time
fixed_stop_times['departure_time'] = fixed_stop_times['departure_time'].dt.time
fixed_stop_times.to_csv('fixed_stop_times.txt', sep=',', index=False)

fixed_trips = pd.DataFrame(fixed_trips_rows)
fixed_trips.to_csv('fixed_trips.txt', sep=',', index=False)

fixed_frequencies = pd.DataFrame(fixed_frequencies_rows)
fixed_frequencies['start_time'] = fixed_frequencies['start_time'].dt.time
fixed_frequencies['end_time'] = fixed_frequencies['end_time'].dt.time
fixed_frequencies.to_csv('fixed_frequencies.txt', sep=',', index=False)
