from shapely.geometry import LineString, Point


class Intersections():
    def __init__(self, trips, shapes):
        self.__trips = trips
        self.__shapes = shapes

    def __check_line_intersections(self, excluded=[]):
        trips_linestrings = {}
        for i, trip_code in enumerate(self.__trips['trip_id']):
            trip = self.__trips[self.__trips.trip_id == trip_code]
            trip_shape = self.__shapes[self.__shapes['shape_id'].isin(trip['shape_id'])]
            trip_lat = trip_shape.shape_pt_lat.tolist()
            trip_lon = trip_shape.shape_pt_lon.tolist()
            trip_linestring = LineString(zip(trip_lon, trip_lat))
            trips_linestrings[trip_code] = trip_linestring

        totals = {}
        for i, trip1_code in enumerate(self.__trips['trip_id']):
            if trip1_code not in excluded:
                inter = []

                trip1_linestring = trips_linestrings[trip1_code]
                for trip2_code in self.__trips['trip_id'][i + 1:]:
                    if trip1_code != trip2_code and trip1_code not in excluded and trip2_code not in excluded:
                        trip2_linestring = trips_linestrings[trip2_code]
                        if trip1_linestring.intersects(trip2_linestring):
                            if not isinstance(trip1_linestring.intersection(trip2_linestring), Point):
                                inter.append(trip2_code)

                totals[trip1_code] = inter

        return totals

    def build_intersections(self, ignored):
        return self.__check_line_intersections(ignored)
