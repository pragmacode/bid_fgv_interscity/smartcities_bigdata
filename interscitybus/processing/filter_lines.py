from collections import Counter


class FilterLines():
    def __init__(self, trips, shapes, bus_lines):
        self.__trips = trips
        self.__shapes = shapes
        self.__bus_lines = bus_lines

    def __is_bus_line(self, trip_id):
        return 'CPTM' not in trip_id and 'METR' not in trip_id

    def __is_late_night_line(self, trip_id):
        return 'N' in trip_id and 'N-' not in trip_id

    def __has_repeated_coordinates(self, coordinates):
        counts = Counter(coordinates)

        for _coordinate, count in counts:
            if count > 2:
                return True

        return False

    def __circular_lines_trip_ids(self):
        trip_ids = []

        for _, line in self.__bus_lines.iterrows():
            if line['lc']:  # lc - linha circular
                trip_id = line['lt'] + '-' + str(line['tl'])+'-'+'0'  # lt - letreiro, tl - tipo de linha
                trip_ids.append(trip_id)

        return trip_ids

    def ignored_trip_ids(self):
        """
        Bus lines with repeated coordinates, late night routes and not bus
        lines (subway, etc) are also excluded.
        """

        ignored = []
        for trip_id in self.__trips['trip_id']:
            if trip_id in ignored:
                continue

            if not self.__is_bus_line(trip_id) or self.__is_late_night_line(trip_id):
                ignored.append(trip_id)
                continue

            trip = self.__trips[self.__trips.trip_id == trip_id]

            trip_shape = self.__shapes[self.__shapes['shape_id'].isin(trip['shape_id'])]
            lat = trip_shape.shape_pt_lat.tolist()
            lon = trip_shape.shape_pt_lon.tolist()
            coordinates = list(zip(lat, lon))

            if self.__has_repeated_coordinates(coordinates):
                ignored.append(trip_id)

        for trip_id in self.__circular_lines_trip_ids():
            if trip_id not in ignored:
                ignored.append(trip_id)

        return ignored
