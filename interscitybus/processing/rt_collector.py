import time
import dateutil.parser
from copy import deepcopy
from sentry_sdk import capture_exception

from interscitybus.processing.data_processor import DataProcessor
from interscitybus.api import OlhoVivoApi


class OlhoVivoRTCollector():
    def collect_rt_data(self, auxdata, timedicts, api_token, dbip=None):
        """
        Collect real time avl data from OlhoVivo online API

        Parameters:
            auxdata: auxiliary data structures from prepare_rt_auxdata function
            timedicts: Memory structure (tuple) to keep information between many
                        calls of collect_rt_data
            api_token: string with OlhoVivo API token
            dbip: MongoDB IP adress. If None (default), data is not inserted on DB.
        """

        logger, nys, distancias_pontos, linesedge, excluded, gIndex, tripss, lsdict, kl_states = auxdata
        overtime, mptimedict = timedicts
        doclist = []

        try:
            api = OlhoVivoApi()
            col_ti = time.time()
            pos = api.get_response('Posicao')
            col_tf = time.time()

            if type(pos) == dict:
                if 'l' in pos.keys():
                    temp = deepcopy(pos)
                    for a in temp['l']:
                        for b in a['vs'][-1::-1]:
                            b['ta'] = dateutil.parser.parse(b['ta'])

                    col_tf = time.time()

                    if temp:
                        doclist, overtime, mptimedict = DataProcessor().buses_position(temp, excluded, tripss, overtime,
                                                                                       mptimedict, nys, lsdict,
                                                                                       distancias_pontos, linesedge,
                                                                                       doclist)

            '''
            @TODO: Need to check our mongo package
            if dbip:
                client = MongoClient(dbip)

                dbltt = client['linktraveltimeRT']
                if len(doclist) > 0:
                    dbltt.ltt.insert_many(doclist, ordered=False)

                if 'pos' in locals() and type(pos) == dict and 'l' in pos.keys() and len(pos['l']) > 0:
                    dbpos = client['rawdata_sptrans']
                    dbpos.pos.insert_many(pos['l'], ordered=False)

                client.close()
            '''
            ctime = col_tf - col_ti
            return (doclist, ctime, (overtime, mptimedict))

        except Exception as e:
            logger.exception('Unhandled Exception localhost')
            capture_exception(e)
            print(e)
            return ([], col_tf - col_ti, (overtime, mptimedict))
        raise
