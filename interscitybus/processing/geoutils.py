from pyproj import CRS, Proj
from geopy.distance import geodesic
from django.conf import settings
from shapely.geometry import LineString

import pandas


def getcrs():
    crs_22523 = CRS('EPSG:22523')
    crs_4326 = CRS("EPSG:4326")
    return (crs_22523, crs_4326)


def getnys():
    nys = Proj('EPSG:22523')
    outProj = Proj("EPSG:4326")
    return (nys, outProj)


def simple_shape_dist(selec_linhas, trips, shapes):
    distance_all_shapes = {}

    '''calcula todas as distancias dos shapes'''

    for trip_id in selec_linhas:

        trip = trips[trips.trip_id == trip_id]
        trip_shape = shapes[shapes['shape_id'].isin(trip['shape_id'])]
        shape_lat = trip_shape.shape_pt_lat.tolist()
        shape_lon = trip_shape.shape_pt_lon.tolist()

        # distancias dos shapes
        lon1 = shape_lon[0]
        lat1 = shape_lat[0]
        line_lenght = [0.]
        dist = [0.]
        for lat2, lon2 in zip(shape_lat[1:], shape_lon[1:]):
            d = geodesic((lat1, lon1), (lat2, lon2)).m
            dist.append(d)
            line_lenght.append(line_lenght[-1] + d)
            lat1 = lat2
            lon1 = lon2
        distance_all_shapes[trip_id] = [shape_lat, shape_lon, line_lenght]
    return distance_all_shapes


def get_linestrings():
    lsdict = {}
    base_path = str(settings.BASE_DIR)
    tripss = pandas.read_csv(base_path + '/data/sao_paulo/gtfs/trips.txt', sep=',')
    shapes = pandas.read_csv(base_path + '/data/sao_paulo/gtfs/shapes.txt', sep=',')

    for trip_id in tripss.trip_id.values:
        trip = tripss[tripss.trip_id == trip_id]
        trip_shape = shapes[shapes['shape_id'].isin(trip['shape_id'])]
        shapelat = trip_shape.shape_pt_lat.tolist()
        shapelon = trip_shape.shape_pt_lon.tolist()

        nys, outProj = getnys()
        s = nys(shapelon, shapelat)
        lat = s[1]
        lon = s[0]
        ls = LineString(zip(lon, lat))
        lsdict[trip_id] = ls
    return lsdict
