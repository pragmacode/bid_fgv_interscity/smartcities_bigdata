import numpy
import datetime
from shapely.geometry import Point
from interscitybus.processing.data_preprocessor import DataPreprocessor


class DataProcessor():
    def __filter(self, collection):
        if len(collection['distance']) > 100:
            del collection['distance'][:50]
            del collection['time'][:50]

    def __last_two_from(self, collection):
        return collection[-2], collection[-1]

    def __update_excluded(self, excluded, od, trips):
        for di in od:
            lkey = di['c'] + '-' + str(di['sl'] - 1)
            if lkey not in excluded and lkey not in trips.trip_id.values:
                excluded.append(lkey)

    def __update_collection_key(self, collection, key):
        if key not in collection.keys():
            collection[key] = {}

    def __update_collection_keys(self, collection, key1, key2):
        if key2 not in collection[key1].keys():
            collection[key1][key2] = {}
            collection[key1][key2]['distance'] = []
            collection[key1][key2]['time'] = []
            collection[key1][key2]['vertex'] = []

    def __bus_time_loop(self, vertices_distances, lines_edges, bus_positions, bus_time, lkey, bkey, t_offset, doc_list):
        mpvec = numpy.array(vertices_distances[lkey]['mid_distance']) * 1000
        ledges = lines_edges[lkey]
        for i, mp in enumerate(mpvec):
            d0, d1 = self.__last_two_from(bus_positions[lkey][bkey]['distance'])
            if d0 <= mp <= d1 and -200 < d1 - d0:
                t0, t1 = self.__last_two_from(bus_positions[lkey][bkey]['time'])
                inverse_speed = (t1 - t0) / (d1 - d0)
                bb = t0 - inverse_speed * d0
                t = inverse_speed * mp + bb
                flag = 0
                if i == len(mpvec) - 1:
                    flag = 1
                bus_time[lkey][bkey]['vertex'].append(ledges[i - flag][0 + flag])
                bus_time[lkey][bkey]['distance'].append(mp)
                bus_time[lkey][bkey]['time'].append(t)
                if len(bus_time[lkey][bkey]['time']) > 1:
                    preprocessor = DataPreprocessor(bus_time, lkey, bkey)
                    doc_list = preprocessor.lttdb_docs(t_offset, ledges, doc_list, mpvec)

    def buses_position(self, avl, excluded, trips, bus_positions, bus_time, nys,
                       routes_shape, vertices_distances, lines_edges, doc_list):
        """
        Estimates the buses position of the colected data

        Parameters:
            avl: latest buses avl data
            excluded: list with excluded bus lines
            trips: Dataframe with GTFS trips.txt data
            bus_positions: Dictionary with buses position
            bus_time: Dictionary with buses times on vertexes
            nys: pyproj Proj object for gps coordinates conversion to meters
            routes_shape: Dictionary with shapely LineString objects of all bus routes
            vertices_distances: Dictionary with all vertexes distances
            lines_edges: Dictionary with the edges of all bus lines
            doc_list: List with all processed documents
        """
        od = sorted(avl['l'], key=lambda i: i['c'])
        self.__update_excluded(excluded, od, trips)

        for di in od:
            lkey = di['c'] + '-' + str(di['sl'] - 1)
            if lkey not in excluded:
                self.__update_collection_key(bus_positions, lkey)
                self.__update_collection_key(bus_time, lkey)
                for dj in di['vs']:
                    bkey = dj['p']
                    self.__update_collection_keys(bus_positions, lkey, bkey)
                    self.__update_collection_keys(bus_time, lkey, bkey)
                    p = Point(nys(dj['px'], dj['py']))
                    if routes_shape[lkey].distance(p) < 100:
                        pdist = routes_shape[lkey].project(p)
                        t_offset = dj['ta'].replace(hour=7, minute=0, second=0)
                        if dj['ta'] < t_offset:
                            t_offset = t_offset - datetime.timedelta(days=1)
                        psec = (dj['ta'] - t_offset).total_seconds()
                        if len(bus_positions[lkey][bkey]['time']) == 0 or bus_positions[lkey][bkey]['time'][-1] != psec:
                            bus_positions[lkey][bkey]['distance'].append(pdist)
                            bus_positions[lkey][bkey]['time'].append(psec)
                            if len(bus_positions[lkey][bkey]['time']) > 1:
                                self.__bus_time_loop(vertices_distances, lines_edges, bus_positions,
                                                     bus_time, lkey, bkey, t_offset, doc_list)
                self.__filter(bus_positions[lkey][bkey])
                self.__filter(bus_time[lkey][bkey])

        return doc_list, bus_positions, bus_time
