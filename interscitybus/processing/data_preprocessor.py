import logging
import pandas
import numpy
import datetime
from django.conf import settings

from interscitybus.processing import geoutils
from interscitybus.models import Graph


class DataPreprocessor():
    def __init__(self, mptimedict, lkey, bkey):
        self.__base_path = str(settings.BASE_DIR)
        self.__mptimedict = mptimedict
        self.__lkey = lkey
        self.__bkey = bkey

    def prepare_rt_auxdata(self):
        """
        Prepare auxiliary data structures for online data colect and process.
        """
        logger = logging.getLogger()
        nys, outProj = geoutils.getnys()

        graph = Graph.objects.last()

        distancias_pontos = graph.retrieve_points_distances()
        linesedge = graph.lines_edges_dict()
        excluded = graph.ignored_lines_list()
        gIndex = graph.to_ntwkx()

        tripss = pandas.read_csv(self.__base_path + '/data/sao_paulo/gtfs/trips.txt', sep=',')
        lsdict = geoutils.get_linestrings()

        edgecol = list(gIndex.edges)
        edgecol = [str(e) for e in edgecol]
        dads = [tuple([numpy.nan for i in range(len(edgecol))])for k in range(5)]

        df0 = pandas.DataFrame(dads, columns=edgecol)

        return logger, nys, distancias_pontos, linesedge, excluded, gIndex, tripss, lsdict, df0

    def init_timedicts(self):
        """
        Initialize memory structure for bus positioning
        """
        return ({}, {})

    def __is_valid(self, last_vertices, edges):
        posi = 0
        e_temp = last_vertices[posi]
        is_valid = False
        for e in edges:
            if e_temp == e[posi]:
                if posi == 1:
                    is_valid = True
                    break
                posi = 1
                e_temp = last_vertices[posi]
        return is_valid

    def __search_edge(self, last_vertices, edges):
        """
        Search for traveled edges between the last two bus positions.

        Parameters:
            last_vertices: tuple with two last crossed vertexes
            edges: list of edges of considered bus line
        """
        is_valid = self.__is_valid(last_vertices, edges)

        vi, vf = last_vertices
        r = []
        s = True
        trial = 0

        while(s):
            if last_vertices in edges:
                r.append((edges.index(last_vertices), last_vertices))
                s = False
            elif is_valid:
                for item in edges:
                    if item[0] == vi:
                        r.append((edges.index(item), item))
                        vi = item[1]
                    if item[1] == vf:
                        s = False
            else:
                s = False
            trial += 1
            if trial > len(edges):
                print('edge not found')
                break

        r = sorted(r, key=lambda x: x[0])
        return r

    def __last_two(self, label):
        mptimedict = self.__mptimedict[self.__lkey][self.__bkey]
        return mptimedict[label][-2], mptimedict[label][-1]

    def __start(self, t_offset, t):
        return t_offset + datetime.timedelta(seconds=t)

    def lttdb_docs(self, t_offset, ledges, doclist, mpvec):
        """
        Estimates the link travel time and prepare documents for MongoDB

        Parameters:
            bkey: String with bus_id
            lkey: String with trip_id
            t_offset: Time variable for daily start time at 1 am (GMT+0)/4 am (GMT-3)
            ledges: list of edges of considered bus line
            doclist: List with all processed documents
            mpvec: Numpy array with the vertexes distances related to lkey bus line
            mptimedict: Dictionary holding time on vertexes of all buses
        """
        u, v = self.__last_two('vertex')

        if u != v:
            edges = self.__search_edge((u, v), ledges)
            if len(edges) == 1:
                t0, t1 = self.__last_two('time')
                document = {'link': str(tuple(edges[0][1])), 'link_index': ledges.index(tuple(edges[0][1])),
                            'trip_id': self.__lkey, 'bus_id': self.__bkey, 'ltt': t1 - t0,
                            'aproxlinkstart': self.__start(t_offset, t0)}
                doclist.append(document)
            elif len(edges) > 1:
                t0, t1 = self.__last_two('time')
                d0, d1 = self.__last_two('distance')
                inverse_speed = (t1 - t0) / (d1 - d0)
                bb = t0 - inverse_speed * d0
                ti = t0
                for e in edges:
                    t = inverse_speed * mpvec[e[0]] + bb
                    document = {'link': str(tuple(e[1])), 'link_index': ledges.index(tuple(e[1])),
                                'trip_id': self.__lkey, 'bus_id': self.__bkey, 'ltt': t - ti,
                                'aproxlinkstart': self.__start(t_offset, ti)}
                    ti = t
                    doclist.append(document)

        return doclist
