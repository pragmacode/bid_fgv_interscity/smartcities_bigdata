import pandas


class StatesEstimator():
    def latest_links(self, auxdata, d0):
        """
        Estimates the link state of the latest data

        Parameters:
            auxdata: auxiliary data structures from prepare_rt_auxdata function
            d0: Dataframe with latest link travel times
        """
        a_ltts = d0[['link', 'ltt']]
        unq = a_ltts.loc[~a_ltts['link'].duplicated(keep=False)].set_index('link')
        dup = a_ltts.loc[a_ltts['link'].duplicated(keep=False)]
        med = dup.groupby('link').median()
        df = pandas.concat([unq, med])
        df = df.T
        dc = df.to_dict('records')
        cl = list(dc[0].keys())
        auxdata[-1][cl] = auxdata[-1][cl].shift(periods=1)
        auxdata[-1].loc[0, cl] = tuple(dc[0].values())
        last_state = auxdata[-1].median()
        return (last_state, auxdata)
