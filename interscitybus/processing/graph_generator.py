import pandas
from django.conf import settings
from interscitybus.api import OlhoVivoApi
from interscitybus.processing.filter_lines import FilterLines
from interscitybus.processing.intersections import Intersections
from interscitybus.processing.line_tools import LineTools
from interscitybus.processing.deduplicator import Deduplicator
from interscitybus.processing.graph_builder import GraphBuilder


class GraphGenerator():
    def __init__(self):
        self.base_path = str(settings.BASE_DIR)
        self.__trips = []
        self.__shapes = []
        self.__stops = []
        self.__stop_times = []
        self.__bus_lines = []

    def load_csvs(self):
        self.__trips = pandas.read_csv(self.base_path + '/data/sao_paulo/gtfs/trips.txt', sep=',')
        if len(self.__trips) == 0:
            raise RuntimeError("Empty trips GTFS")

        self.__shapes = pandas.read_csv(self.base_path + '/data/sao_paulo/gtfs/shapes.txt', sep=',')
        if len(self.__shapes) == 0:
            raise RuntimeError("Empty shapes GTFS")

        self.__stops = pandas.read_csv(self.base_path + '/data/sao_paulo/gtfs/stops.txt', sep=',')
        if len(self.__stops) == 0:
            raise RuntimeError("Empty stops GTFS")

        self.__stop_times = pandas.read_csv(self.base_path + '/data/sao_paulo/gtfs/stop_times.txt', sep=',')
        if len(self.__stop_times) == 0:
            raise RuntimeError("Empty stop_times GTFS")

    def load_bus_lines(self):
        api = OlhoVivoApi()

        self.__bus_lines = api.bus_lines()

    def calculate_ignored_trips(self):
        return FilterLines(self.__trips, self.__shapes, self.__bus_lines).ignored_trip_ids()

    def calculate_intersections(self, ignored_trip_ids):
        return Intersections(self.__trips, self.__shapes).build_intersections(ignored_trip_ids)

    def calculate_distance_points(self, ignored_trip_ids):
        return LineTools(self.__trips, self.__shapes,
                         self.__stops, self.__stop_times).calculate_line_distances(ignored_trip_ids)

    def dist_points_deduplicated(self, distances_points, intersections):
        return Deduplicator().deduplicate_vertices(distances_points, intersections, self.__trips, self.__shapes)

    def build_graph_with(self, distances_points_deduplicated):
        return GraphBuilder().build_graph(distances_points_deduplicated)
