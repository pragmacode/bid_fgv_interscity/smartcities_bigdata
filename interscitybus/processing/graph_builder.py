import networkx

from interscitybus.models import Graph


class GraphBuilder():
    def __add_node(self, node):
        self.__g.add_node(node)
        self.__g.nodes[node]['index'] = self.__vxi
        self.__pos[node] = node
        self.__gIndex.add_node(self.__vxi)
        self.__gIndex.nodes[self.__vxi]['pos'] = node
        self.__vxi += 1

    def __add_edge(self, d, i, u, v):
        disttemp = d['mid_distance'][i + 1] - d['mid_distance'][i]
        self.__g.add_edge(u, v)
        self.__g.edges[u, v]['distance'] = disttemp
        self.__gIndex.add_edge(self.__g.nodes[u]['index'], self.__g.nodes[v]['index'])
        self.__gIndex.edges[self.__g.nodes[u]['index'], self.__g.nodes[v]['index']]['distance'] = disttemp

    def __build_graph(self, distances_points):
        """
        Build two auxiliary graphs of the bus lines.

        Parameters:
            distances_points: Dictionary with all vertices distances

        Return:
        g : graph which each vertex is a tuple (lon, lat) and has an interger as
            'index' property

        gIndex : graph which each vertex is an integer and has a tuple (lon, lat) as
                 'pos' property.
                 This graph makes it easier to acess each vertex and edges. Use:
                     gIndex.nodes[560]
                     > {'pos': (-46.62559432924134, -23.525446946815237)}
                     gIndex.in_edges(560)
                     > InEdgeDataView([(559, 560)])
                     gIndex.out_edges(560)
                     > OutEdgeDataView([(560, 561), (560, 620)])

        linesedges : dictonary which the keys are bus lines ids and value is a list
                     with the edges of this line
        """
        self.__g = networkx.DiGraph()
        self.__gIndex = networkx.DiGraph()
        self.__vxi = 0
        self.__pos = {}

        lines_edges = {}
        for line, d in distances_points.items():
            lines_edges[line] = []
            for i, u in enumerate(d['mid_points'][:-1]):
                if not self.__g.has_node(u):
                    self.__add_node(u)
                v = d['mid_points'][i + 1]
                if not self.__g.has_node(v):
                    self.__add_node(v)
                if not self.__g.has_edge(u, v) and u != v:
                    self.__add_edge(d, i, u, v)
                if u != v:
                    lines_edges[line].append((self.__g.nodes[u]['index'], self.__g.nodes[v]['index']))

        return lines_edges

    def build_graph(self, distances_points_fixed):
        lines_edges = self.__build_graph(distances_points_fixed)

        graph = Graph.create_from_ntwkx(self.__gIndex)
        graph.create_lines_edges(lines_edges)
        graph.create_points_distances_and_line_string(distances_points_fixed)

        return graph
