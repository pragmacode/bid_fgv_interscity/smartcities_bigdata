from interscitybus.processing import geoutils
from pyproj import Transformer
from interscitybus.processing.line_tools import LineTools
from interscitybus.processing.distances import Distances


class Deduplicator():
    def __calculate_dbp(self):
        p1 = self.__list_mid_points[0]
        i = 0
        self.__dbp = []
        for p2 in self.__list_mid_points[1:]:
            if p1.distance(p2) == 0:
                self.__dbp.append(i + 1)
            i += 1
            p1 = p2

    def __calculate_lmp(self):
        self.__calculate_dbp()
        self.__lmp = self.__list_mid_points

        if len(self.__dbp) > 0:
            dbp = frozenset(self.__dbp)
            self.__lmp = [i for j, i in enumerate(self.__list_mid_points) if j not in dbp]

    def __fix_stops_distance(self, line, distances_points, trips, shapes):
        """
        Recalculate distances for modified bus lines

        Parameters:
            trips: Dataframe with all trips (from gtfs trips.txt)
            shapes : Dataframe with all shapes (from gtfs shapes.txt)
            excluded: List with all excluded bus lines
        """
        self.__list_mid_points = distances_points[line]['mid_points_proj']
        self.__calculate_lmp()

        crsnys, crsoriginal = geoutils.getcrs()
        trans2geo = Transformer.from_crs(crsnys, crsoriginal, always_xy=True)
        lmp2 = [trans2geo.transform(p.x, p.y) for p in self.__lmp]
        stopslon, stopslat = zip(*lmp2)

        lat, lon = LineTools(trips, shapes).get_line_coordinates(line)
        totald = Distances().all_distances(line, trips, shapes, lat, lon, stopslat, stopslon)

        distances_points[line]['mid_points_proj'] = self.__lmp
        distances_points[line]['mid_points'] = lmp2
        distances_points[line]['mid_lats'] = stopslat
        distances_points[line]['mid_lons'] = stopslon
        distances_points[line]['mid_distance'] = [i / 1000 for i in totald]
        return distances_points

    def __modified_points(self, distances_points, intersections, eps=1e-5):
        self.__modified = []
        for t1, v in intersections.items():
            lmp1 = distances_points[t1]['mid_points_proj']
            ls1 = distances_points[t1]['proj_ls']
            for t2 in v:
                lmp2 = distances_points[t2]['mid_points_proj']
                ls2 = distances_points[t2]['proj_ls']
                for kk, p1 in enumerate(lmp1):
                    if ls1.distance(p1) < eps and ls2.distance(p1) < eps:
                        for ll, p2 in enumerate(lmp2):
                            if ls1.distance(p2) < eps and ls2.distance(p2) < eps:
                                if p1 != p2 and p1.distance(p2) < 10:
                                    distances_points[t2]['mid_points_proj'][ll] = p1
                                    if t2 not in self.__modified:
                                        self.__modified.append(t2)

    def __remove_duplicated_vertices(self, distances_points, intersections, trips, shapes):
        """
        Implements a reduction in the number of vertices for the final graph.
        Given two different, intersecting bus lines, if two different vertices belongs
        to the two bus line shapes simultaneously and are closer than 10 meters,
        the second vertex becomes the same as the first.

        Parameters:
            distances_points: Dictionary with all vertices distances
            intersections: Dictionary with all intersections between bus lines
            trips: Dataframe with all trips (from gtfs trips.txt)
            shapes : Dataframe with all shapes (from gtfs shapes.txt)
        """

        self.__modified_points(distances_points, intersections)

        for t1 in self.__modified:
            distances_points = self.__fix_stops_distance(t1, distances_points, trips, shapes)

        return distances_points

    def deduplicate_vertices(self, distances_points, intersections, trips, shapes):
        distances_points_f = self.__remove_duplicated_vertices(distances_points, intersections, trips, shapes)
        return distances_points_f
