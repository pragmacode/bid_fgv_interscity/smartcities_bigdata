import numpy
from interscitybus.processing import geoutils
from geopy.distance import geodesic


class Distances():
    def __total_distance(self, lat, lon, stops_lat, stops_lon, line_length, offset):
        total_dist = [0] * len(stops_lat)
        total_k = 0
        for i, _ in enumerate(line_length[:-1]):
            k_list = list(range(len(stops_lat)))
            for k in k_list[total_k: total_k + offset]:
                bus_stop_int = numpy.array([geodesic((stops_lat[k], stops_lon[k]), (lat8, lon8)).m for lat8, lon8 in
                                            zip(lat[i:i + 2], lon[i:i + 2])])
                if (line_length[i] + bus_stop_int[0]) - (line_length[i + 1] - bus_stop_int[1]) < 1:
                    if line_length[i] + bus_stop_int[0] <= line_length[i + 1]:
                        if k == total_k:
                            total_dist[k] = line_length[i] + bus_stop_int[0]
                            total_k += 1

        return total_dist

    def all_distances(self, line, trips, shapes, lat, lon, stopslat, stopslon):
        distances = geoutils.simple_shape_dist([line], trips, shapes)
        bus_line_length = distances[line][2]
        offset = 1
        while True:
            total_dist = self.__total_distance(lat, lon, stopslat, stopslon, bus_line_length, offset)
            current_stops = numpy.array([total_dist[i + 1] - k for i, k in enumerate(total_dist[:-1])])
            if (any(x < 0 for x in current_stops) and offset < 10) or all(current_stops == 0):
                offset += 1
            else:
                break
        return total_dist
