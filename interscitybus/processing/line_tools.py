from pyproj import Transformer
from shapely.geometry import LineString, Point
from geopy.distance import great_circle
from sentry_sdk import capture_exception

from interscitybus.processing import geoutils
from interscitybus.processing.distances import Distances


class LineTools():
    def __init__(self, trips, shapes, stops=[], stop_times=[]):
        """
        trips: Dataframe with all trips (from gtfs trips.txt)
        shapes: Dataframe with all shapes (from gtfs shapes.txt)
        stops: Dataframe with all bus stops (from gtfs stops.txt)
        stop_times: Dataframe with all bus stops per line (from gtfs stop_times.txt)
        """
        self.__trips = trips
        self.__shapes = shapes
        self.__stops = stops
        self.__stop_times = stop_times

        self.__stops_lat0 = []
        self.__stops_lon0 = []

    def __set_stops_coordinates(self, line):
        line_id = self.__stop_times[self.__stop_times.trip_id == line]
        trip_stops = self.__stops[self.__stops['stop_id'].isin(line_id['stop_id'])]
        trip_stops = trip_stops.set_index('stop_id')
        trip_stops = trip_stops.reindex(index=line_id['stop_id'])

        self.__stops_lat0 = trip_stops.stop_lat.tolist()
        self.__stops_lon0 = trip_stops.stop_lon.tolist()

    def get_line_coordinates(self, line):
        ida = self.__trips[self.__trips.trip_id == line]
        trip_shape = self.__shapes[self.__shapes['shape_id'].isin(ida['shape_id'])]
        lat = trip_shape.shape_pt_lat.tolist()
        lon = trip_shape.shape_pt_lon.tolist()
        return lat, lon

    def __fix_coordinates(self, lat, lon):
        line_path = LineString(zip(lon, lat))
        original_coordinates = zip(self.__stops_lon0, self.__stops_lat0)
        projection_of_stops = [line_path.interpolate(line_path.project(Point(x, y))) for x, y in original_coordinates]
        fixed_stops_coordinates = list(zip(*[(p.x, p.y) for p in projection_of_stops]))
        self.__fixed_stops_lon = list(fixed_stops_coordinates[0])
        self.__fixed_stops_lat = list(fixed_stops_coordinates[1])

    def __filter_nearby_stops(self, lat, lon):
        self.__fix_coordinates(lat, lon)
        close_bus_stops = []
        for i in range(len(self.__fixed_stops_lat) - 1):
            dp = great_circle((self.__fixed_stops_lat[i], self.__fixed_stops_lon[i]),
                              (self.__fixed_stops_lat[i + 1], self.__fixed_stops_lon[i + 1])).m
            if dp < 10:  # meters
                close_bus_stops.append(i + 1)

        self.__filtered_stops_lat = self.__fixed_stops_lat
        self.__filtered_stops_lon = self.__fixed_stops_lon

        if len(close_bus_stops) > 0:
            close_bus_stops = frozenset(close_bus_stops)
            self.__filtered_stops_lat = [i for j, i in enumerate(self.__fixed_stops_lat) if j not in close_bus_stops]
            self.__filtered_stops_lon = [i for j, i in enumerate(self.__fixed_stops_lon) if j not in close_bus_stops]

    def __calculate_new_coords(self, trans2geo, lp):
        lp2 = [trans2geo.transform(p.x, p.y) for p in lp]
        lon, lat = zip(*lp2)
        return lp2, lat, lon

    def __calculate_mid_points(self, total_d):
        mid_points = []
        p1 = total_d[0]
        for p2 in total_d[1:]:
            mid_points.append(p1 + (p2 - p1) / 2)
            p1 = p2
        self.__mid_points = [i / 1000 for i in mid_points]

    def __line_stops_distances(self, line):
        """
        Calculates the bus stops distances along a single bus line

        Parameters:
            line: String with a sinngle trip_id
        """
        self.__set_stops_coordinates(line)
        lat, lon = self.get_line_coordinates(line)
        self.__filter_nearby_stops(lat, lon)
        total_d = Distances().all_distances(line, self.__trips, self.__shapes, lat, lon,
                                            self.__filtered_stops_lat, self.__filtered_stops_lon)
        self.__calculate_mid_points(total_d)

        dpontos = [i / 1000 for i in total_d]

        # This snippet here is kinda sus!
        templ = [self.__mid_points[i + 1] - k for i, k in enumerate(self.__mid_points[:-1])]
        if any(x < 0 for x in templ):
            return total_d

        crsnys, crsoriginal = geoutils.getcrs()
        trans2metric = Transformer.from_crs(crsoriginal, crsnys, always_xy=True)
        s = trans2metric.transform(lon, lat)
        ls = LineString(zip(s[0], s[1]))

        ldp = [ls.interpolate(p * 1000) for p in dpontos]
        lmp = [ls.interpolate(p * 1000) for p in self.__mid_points]
        trans2geo = Transformer.from_crs(crsnys, crsoriginal, always_xy=True)

        lmp2, newlon, newlat = self.__calculate_new_coords(trans2geo, lmp)
        ldp2, snewlat, snewlon = self.__calculate_new_coords(trans2geo, ldp)

        resposta = {}
        resposta['exc_distance'] = dpontos
        resposta['mid_distance'] = self.__mid_points

        resposta['exc_points_proj'] = ldp
        resposta['mid_points_proj'] = lmp

        resposta['exc_points'] = ldp2
        resposta['mid_points'] = lmp2

        resposta['exc_lats'] = snewlat
        resposta['exc_lons'] = snewlon

        resposta['mid_lats'] = newlat
        resposta['mid_lons'] = newlon
        resposta['proj_ls'] = ls

        return resposta, total_d

    def __stops_distances(self, excluded=[]):
        distances_points = {}
        for line in self.__trips['trip_id']:
            if line not in excluded:
                try:
                    r, nn = self.__line_stops_distances(line)
                    distances_points[line] = r
                except Exception as exception:
                    capture_exception(exception)
                    print(exception)
                    pass
        print(distances_points)
        return distances_points

    def calculate_line_distances(self, ignored):
        return self.__stops_distances(ignored)
