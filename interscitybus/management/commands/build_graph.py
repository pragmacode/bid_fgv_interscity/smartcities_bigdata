from django.core.management.base import BaseCommand

from interscitybus.processing.graph_generator import GraphGenerator


class Command(BaseCommand):
    help = 'Builds the graph from GTFS data'

    def handle(self, *args, **options):
        graph_generator = GraphGenerator()
        graph_generator.load_csvs()
        graph_generator.load_bus_lines()
        ignored_trip_ids = graph_generator.calculate_ignored_trips()
        intersections = graph_generator.calculate_intersections(ignored_trip_ids)
        distances_points = graph_generator.calculate_distance_points(ignored_trip_ids)
        distances_points_deduplicated = graph_generator.dist_points_deduplicated(distances_points, intersections)
        graph = graph_generator.build_graph_with(distances_points_deduplicated)
        graph.create_ignored_lines(ignored_trip_ids)
