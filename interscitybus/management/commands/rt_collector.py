from django.core.management.base import BaseCommand

from interscitybus.processing.rt_collector import OlhoVivoRTCollector
from interscitybus.processing.states_estimator import StatesEstimator
from interscitybus.processing.data_preprocessor import DataPreprocessor
from interscitybus.api import OlhoVivoApi
from interscitybus.models import BusGraphPosition

import pandas
import time
from sentry_sdk import capture_exception


class Command(BaseCommand):
    help = 'Builds the graph from GTFS data'

    def handle(self, *args, **options):
        api = OlhoVivoApi()
        api_token = api.token()
        realtime_collector = OlhoVivoRTCollector()
        preprocessor = DataPreprocessor(None, None, None)

        print('preparing aux')
        auxdata = preprocessor.prepare_rt_auxdata()
        timedicts = preprocessor.init_timedicts()
        datadf = pandas.DataFrame()
        d0 = pandas.DataFrame(columns=['link'])
        print('starting loop')
        horas = {}
        i = 0
        while True:
            ovt0 = time.time()
            horario = time.ctime()
            # sem ip do mongodb os dados não são gravados
            docs, ctime, timedicts = realtime_collector.collect_rt_data(auxdata, timedicts, api_token, None)
            ovtf = time.time()
            print('online collect time ', ctime)
            print('online process time ', (ovtf - ovt0) - ctime)
            print('online retrieve+process time ', ovtf - ovt0)

            if len(docs) > 0:
                d0 = pandas.DataFrame(docs)
                lst0 = time.time()
                lstate, auxdata = StatesEstimator().latest_links(auxdata, d0)
                lstf = time.time()
                print('last state time ', lstf - lst0)

                datadf = pandas.concat([datadf, d0])

            print('sleep start. docs: ', len(docs), ' links: ', len(d0.link.value_counts()))
            horas[i] = horario
            for doc in docs:
                try:
                    BusGraphPosition.objects.create(
                        link=doc['link'],
                        link_index=doc['link_index'],
                        link_travel_time=doc['ltt'],
                        link_aprox_started_at=doc['aproxlinkstart'],
                        trip_id=doc['trip_id'],
                        bus_id=doc['bus_id'],
                    )
                except Exception as e:
                    capture_exception(e)
                    print(e)

            print('docs saved to mongodb')
            time.sleep(40)
            i += 1
