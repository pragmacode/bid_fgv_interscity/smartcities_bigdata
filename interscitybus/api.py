import requests
import pandas


from django.conf import settings


class OlhoVivoApi():
    def token(self):
        return settings.OLHO_VIVO_TOKEN

    def __url(self):
        return settings.OLHO_VIVO_URL

    def __session(self):
        session = requests.Session()
        method = 'Login/Autenticar?token=%s' % self.token()
        session.post(self.__url() + method)

        return session

    def get_response(self, path):
        session = self.__session()
        response = session.get(self.__url() + path)
        session.close()

        return response.json()

    def bus_lines(self):
        # add 'N','C','M' for noturn lines, cptm trains and subway
        bus_lines_list = []
        for i in range(10):
            bus_lines_list += self.get_response('Linha/Buscar?termosBusca=%s' % i)

        bus_lines_dataframe = pandas.DataFrame.from_records(bus_lines_list)

        for col in ['lc', 'lt', 'tl']:
            if col not in bus_lines_dataframe:
                raise RuntimeError('Missing {} column in bus_lines'.format(col))

        return bus_lines_dataframe
