from django.db import models

from networkx import DiGraph
from shapely.geometry import LineString, Point


class BusGraphPosition(models.Model):
    # TODO: remove the default and make it a FK when implementing
    # https://gitlab.com/pragmacode/bid_fgv_interscity/smartcities_bigdata/-/issues/18
    graph_id = models.IntegerField(null=False, default=0)
    # TODO: remove the default when we start supporting multiple cities
    city = models.CharField(null=False, blank=False, max_length=100, default='sao_paulo')
    link = models.CharField(null=False, max_length=100, blank=False)
    link_index = models.IntegerField(null=False)
    link_travel_time = models.FloatField(null=False)
    link_aprox_started_at = models.DateTimeField(null=False)
    trip_id = models.CharField(null=False, max_length=100, blank=False)
    bus_id = models.IntegerField(null=False)


class Graph(models.Model):
    city = models.CharField(null=False, blank=False, max_length=100, default='sao_paulo')

    @classmethod
    def create_from_ntwkx(cls, ntwkx_graph, city='sao_paulo'):
        graph = cls.objects.create()

        nodes = []
        for num, data in ntwkx_graph.nodes.items():
            lat, lon = data['pos']
            nodes.append(Node(num=num, lat=lat, lon=lon, graph=graph))
        Node.objects.bulk_create(nodes)

        edges = []
        for node_nums, data in ntwkx_graph.edges.items():
            distance = data['distance']
            node1_num, node2_num = node_nums
            edges.append(Edge(node1_num=node1_num, node2_num=node2_num, distance=distance, graph=graph))
        Edge.objects.bulk_create(edges)

        return graph

    def to_ntwkx(self):
        ntwkx_graph = DiGraph()

        for node in self.node_set.all():
            ntwkx_graph.add_node(node.num, pos=(node.lat, node.lon))

        for edge in self.edge_set.all():
            ntwkx_graph.add_edge(edge.node1_num, edge.node2_num, distance=edge.distance)

        return ntwkx_graph

    def ignored_lines_list(self):
        return [ig_line.trip_id for ig_line in self.ignoredline_set.all()]

    def create_ignored_lines(self, trip_ids):
        for trip_id in trip_ids:
            self.ignoredline_set.create(trip_id=trip_id)

    def create_lines_edges(self, lines_edges_dict):
        for trip_id, edges in lines_edges_dict.items():
            for edge in edges:
                node1_num, node2_num = edge
                self.lineedge_set.create(trip_id=trip_id, node1_num=node1_num, node2_num=node2_num)

    def lines_edges_dict(self):
        lines_edges_dict = dict()
        lines_edges = self.lineedge_set.all()

        for line_edge in lines_edges:
            lines_edges_dict.setdefault(line_edge.trip_id, []).append((line_edge.node1_num, line_edge.node2_num))

        return lines_edges_dict

    def retrieve_points_distances(self):
        points_distances_dict = dict()
        points_distances = self.pointdistance_set.all()

        for p in points_distances:
            points_distances_dict.setdefault(p.trip_id, {})
            self.set_default_dicts(points_distances_dict[p.trip_id], p.kind)
            points_distances_dict[p.trip_id][p.kind + '_distance'].append(p.distance)
            points_distances_dict[p.trip_id][p.kind + '_points_proj'].append(Point(p.point_proj_x_coord,
                                                                                   p.point_proj_y_coord))
            points_distances_dict[p.trip_id][p.kind + '_points'].append((p.point_x_coord, p.point_y_coord))
            points_distances_dict[p.trip_id][p.kind + '_lats'] += (p.lat,)
            points_distances_dict[p.trip_id][p.kind + '_lons'] += (p.lon,)

        self.retrieve_linestrings(points_distances_dict)
        return points_distances_dict

    def retrieve_linestrings(self, points_distances_dict):
        coords_list_dict = dict()
        for ls_coord in self.linestringcoord_set.all():
            coords_list_dict.setdefault(ls_coord.trip_id, []).append((ls_coord.x, ls_coord.y))

        for trip_id in coords_list_dict:
            points_distances_dict[trip_id]['proj_ls'] = LineString(coords_list_dict[trip_id])

    def set_default_dicts(self, base, kind):
        base.setdefault(kind + '_distance', [])
        base.setdefault(kind + '_points_proj', [])
        base.setdefault(kind + '_points', [])
        base.setdefault(kind + '_lats', ())
        base.setdefault(kind + '_lons', ())

    def __build_point_distance(self, trip_id, kind, values, i_pt):
        p_proj_x_coord, p_proj_y_coord = values[kind + '_points_proj'][i_pt].x, values[kind + '_points_proj'][i_pt].y
        p_x_coord, p_y_coord = values[kind + '_points'][i_pt][0], values[kind + '_points'][i_pt][1]

        return PointDistance(
            graph=self,
            trip_id=trip_id,
            kind=kind,
            distance=values[kind + '_distance'][i_pt],
            point_proj_x_coord=p_proj_x_coord,
            point_proj_y_coord=p_proj_y_coord,
            point_x_coord=p_x_coord,
            point_y_coord=p_y_coord,
            lat=values[kind + '_lats'][i_pt],
            lon=values[kind + '_lons'][i_pt]
        )

    def __build_linestring(self, trip_id, point):
        return LineStringCoord(
            graph=self,
            trip_id=trip_id,
            x=point[0],
            y=point[1]
        )

    def create_points_distances_and_line_string(self, distances_points_dict):
        for trip_id, values in distances_points_dict.items():
            points_distances = []
            line_string = []
            for kind in PointDistance.KIND_CHOICES:
                for i_point in range(len(values[kind[0] + '_distance'])):
                    points_distances.append(self.__build_point_distance(trip_id, kind[0], values, i_point))
            for point in values['proj_ls'].coords:
                line_string.append(self.__build_linestring(trip_id, point))
            PointDistance.objects.bulk_create(points_distances)
            LineStringCoord.objects.bulk_create(line_string)


class Node(models.Model):
    num = models.IntegerField(null=False)
    graph = models.ForeignKey(Graph, on_delete=models.CASCADE)
    lat = models.FloatField(null=False)
    lon = models.FloatField(null=False)


class Edge(models.Model):
    node1_num = models.IntegerField(null=False)
    node2_num = models.IntegerField(null=False)
    graph = models.ForeignKey(Graph, on_delete=models.CASCADE)
    distance = models.FloatField(null=False)


class IgnoredLine(models.Model):
    graph = models.ForeignKey(Graph, on_delete=models.CASCADE)
    trip_id = models.CharField(null=False, max_length=50, blank=False)


class LineEdge(models.Model):
    graph = models.ForeignKey(Graph, on_delete=models.CASCADE)
    trip_id = models.CharField(null=False, max_length=50, blank=False)
    node1_num = models.IntegerField(null=False)
    node2_num = models.IntegerField(null=False)


class PointDistance(models.Model):
    EXC_KIND = 'exc'
    MID_KIND = 'mid'
    KIND_CHOICES = [(EXC_KIND, 'exc'), (MID_KIND, 'mid')]

    graph = models.ForeignKey(Graph, on_delete=models.CASCADE)
    trip_id = models.CharField(null=False, max_length=50, blank=False)
    kind = models.CharField(null=False, max_length=10, blank=False, choices=KIND_CHOICES)
    distance = models.FloatField(null=False)
    point_proj_x_coord = models.FloatField(null=False)
    point_proj_y_coord = models.FloatField(null=False)
    point_x_coord = models.FloatField(null=False)
    point_y_coord = models.FloatField(null=False)
    lat = models.FloatField(null=False)
    lon = models.FloatField(null=False)


class LineStringCoord(models.Model):
    graph = models.ForeignKey(Graph, on_delete=models.CASCADE)
    trip_id = models.CharField(null=False, max_length=50, blank=False)
    x = models.FloatField(null=False)
    y = models.FloatField(null=False)
