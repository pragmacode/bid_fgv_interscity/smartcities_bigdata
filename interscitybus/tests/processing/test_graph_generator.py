import pytest
from unittest.mock import call


from interscitybus.processing.graph_generator import GraphGenerator


@pytest.fixture
def subject():
    return GraphGenerator()


def test_attrs(subject):
    assert hasattr(subject, 'base_path') is True
    assert hasattr(subject, '_GraphGenerator__trips') is True
    assert hasattr(subject, '_GraphGenerator__shapes') is True
    assert hasattr(subject, '_GraphGenerator__stops') is True
    assert hasattr(subject, '_GraphGenerator__stop_times') is True


def test_load_csvs(subject, mocker):
    base_path = subject.base_path
    mocked_pandas = mocker.patch('interscitybus.processing.graph_generator.pandas')

    subject.load_csvs()

    mocked_pandas.read_csv.assert_has_calls([
        call(base_path + '/data/sao_paulo/gtfs/trips.txt', sep=','),
        call(base_path + '/data/sao_paulo/gtfs/shapes.txt', sep=','),
        call(base_path + '/data/sao_paulo/gtfs/stops.txt', sep=','),
        call(base_path + '/data/sao_paulo/gtfs/stop_times.txt', sep=','),
    ])

    assert subject._GraphGenerator__trips == mocked_pandas.read_csv.return_value
    assert subject._GraphGenerator__shapes == mocked_pandas.read_csv.return_value
    assert subject._GraphGenerator__stops == mocked_pandas.read_csv.return_value
    assert subject._GraphGenerator__stop_times == mocked_pandas.read_csv.return_value


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_load_bus_lines():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_calculate_ignored_trips():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_calculate_intersections():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_calculate_distance_points():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_dist_points_deduplicated():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_build_graph_with():
    pass
