import pytest
from interscitybus.processing.line_tools import LineTools


@pytest.fixture
def subject():
    return LineTools()


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___set_stops_coordinates():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___get_line_coordinates():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___fix_coordinates():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___filter_nearby_stops():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___get_new_coords():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___calculate_mid_points():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___line_stops_distances():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___stops_distances():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_calculate_line_distances():
    pass
