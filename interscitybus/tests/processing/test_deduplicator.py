import pytest
from interscitybus.processing.deduplicator import Deduplicator


@pytest.fixture
def subject():
    return Deduplicator()


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___fix_stops_distance():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___remove_duplicated_vertices():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_deduplicate_vertices():
    pass
