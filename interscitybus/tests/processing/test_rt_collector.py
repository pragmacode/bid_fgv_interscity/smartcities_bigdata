import pytest
from interscitybus.processing.rt_collector import OlhoVivoRTCollector


@pytest.fixture
def subject():
    return OlhoVivoRTCollector()


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_collect_rt_data():
    pass
