import pytest
from interscitybus.processing.filter_lines import FilterLines


@pytest.fixture
def subject():
    return FilterLines()


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___is_bus_line():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___is_late_night_line():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___has_repeated_coordinates():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test___circular_lines_trip_ids():
    pass


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_ignored_trip_ids():
    pass
