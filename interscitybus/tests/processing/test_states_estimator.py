import pytest
from interscitybus.processing.states_estimator import StatesEstimator


@pytest.fixture
def subject():
    return StatesEstimator()


@pytest.mark.skip(reason='The future is due to 2021-03-08 and I think there will be no time to test it now')
def test_latest_links():
    pass
