import pytest

from interscitybus.management.commands.build_graph import Command


@pytest.fixture
def subject():
    return Command()


def test_handle(subject, mocker):
    graph_generator_class_mock = mocker.patch('interscitybus.management.commands.build_graph.GraphGenerator')
    graph_generator_mock = graph_generator_class_mock.return_value

    graph_mock = graph_generator_mock.build_graph_with.return_value

    subject.handle()

    graph_generator_class_mock.assert_called()
    graph_generator_mock.load_csvs.assert_called()
    graph_generator_mock.load_bus_lines.assert_called()
    graph_generator_mock.calculate_ignored_trips.assert_called()
    graph_generator_mock.calculate_intersections.assert_called()
    graph_generator_mock.calculate_distance_points.assert_called()
    graph_generator_mock.dist_points_deduplicated.assert_called()
    graph_generator_mock.build_graph_with.assert_called()
    graph_mock.create_ignored_lines.assert_called_with(graph_generator_mock.calculate_ignored_trips.return_value)
