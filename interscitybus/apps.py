from django.apps import AppConfig


class InterscitybusConfig(AppConfig):
    name = 'interscitybus'
