# Generated by Django 3.1.7 on 2021-03-31 20:32

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('interscitybus', '0005_pointdistance'),
    ]

    operations = [
        migrations.CreateModel(
            name='LineStringCoord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('trip_id', models.CharField(max_length=50)),
                ('x', models.FloatField()),
                ('y', models.FloatField()),
                ('graph', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='interscitybus.graph')),
            ],
        ),
    ]
